import {ISerializationConfig, IDeserializationConfig} from './configuration';
import {
  IDeserializationSettingsBuilder,
  ISerializationSettingsBuilder,
  DeserializationSettingsBuilder,
  SerializationSettingsBuilder
} from './settingsBuilder';

////////////////////////////////////////////////////////////////////////////////
// Config
////////////////////////////////////////////////////////////////////////////////


export {
  IConfig,
  ISerializationConfig,
  IDeserializationConfig,
  SerializationConfig,
  DeserializationConfig
} from './configuration';

export { StrategyForUnknownProperty } from './enums';

/**
 * Utility class.
 */
export class ConfigurationUtil {

  // Default config.
  private static _desConfig: IDeserializationConfig = DeserializationSettingsBuilder.DEFAULT;
  private static _serConfig: ISerializationConfig = SerializationSettingsBuilder.DEFAULT;

  /**
   * Gets the global configuration for searilation process.
   */
  public static get globalSerConfig(): ISerializationConfig {
    return ConfigurationUtil._serConfig;
  }

  /**
   * Overrides the global configuration for searilation process.
   */
  public static set globalSerConfig(v: ISerializationConfig) {
    ConfigurationUtil._serConfig = v;
  }

  /**
   * Gets the global configuration for desearilation process.
   */
  public static get globalDesConfig(): IDeserializationConfig {
    return ConfigurationUtil._desConfig;
  }

  /**
   * Overrides the global configuration for desearilation process.
   */
  public static set globalDesConfig(v: IDeserializationConfig) {
    ConfigurationUtil._desConfig = v;
  }

  /**
   * Resets the global config to its predefined values.
   */
  public static resetGlobalConfig() {
    ConfigurationUtil._desConfig = DeserializationSettingsBuilder.DEFAULT;
    ConfigurationUtil._serConfig = SerializationSettingsBuilder.DEFAULT;
  }

  public static get settingsBuilderFor(): {
    DESERIALIZER: IDeserializationSettingsBuilder,
    SERIALIZER: ISerializationSettingsBuilder
  } {
    return {
      DESERIALIZER: new DeserializationSettingsBuilder(),
      SERIALIZER: new SerializationSettingsBuilder()
    };
  }


  public static DEFAULTS: {
    BOOLEAN: boolean,
    STRING: string,
    NUMBER: number
  } = {
    BOOLEAN: false,
    STRING: '',
    NUMBER: 0,
  };


}

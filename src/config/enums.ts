
/**
 * Enum to define how to process the unknown properties.
 */
export enum StrategyForUnknownProperty {
  /**
   * If any unknown property appears, then throw an error;
   */
  THROW_ERROR,
  /**
   * Do not put the unknown property in the final result.
   */
  IGNORE,
  /**
   * Put the unknown property in the final result.
   */
  KEEP
}

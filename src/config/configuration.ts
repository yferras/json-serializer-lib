import {FunctionUtil, IFunction} from "../func";
import {StrategyForUnknownProperty} from "./enums";

/**
 * Interface for settings.
 */
export interface IConfig {
  /**
   * To include (or not) the default values.
   * @returns {@code true} if and only if the result should have the properties
   *          defined with default values; otherwise returns {@code false}.
   */
  get includeDefaultValues(): boolean;
  /**
   * To include (or not) the stats in the result.
   * @returns {@code true} if and only if the result should have the stats values;
   *          otherwise returns {@code false}.
   */
  get includeStats(): boolean;

  /**
   * Gets the list of predefined defaults values.
   */
  get defaultValues(): unknown[];

  get includeEmptyArrays(): boolean;

  get includeEmptyObjects(): boolean;

  get includeNullValues(): boolean;

  get includeUndefinedValues(): boolean;

  get includeDefaultBoolean(): boolean;

  get includeDefaultString(): boolean;

  get includeDefaultNumber(): boolean;

  get includeUserDefinedDefaultValues(): boolean;

  get strategyForUnknownProperty(): StrategyForUnknownProperty;

  get converterMap(): Record<string, IFunction<unknown, unknown>>;

}

export interface ISerializationConfig extends IConfig {

  get fromBooleanTo(): IFunction<boolean, unknown>;

  get fromDateTo(): IFunction<Date, unknown>;

}

export interface IDeserializationConfig extends IConfig {

  get toBooleanFrom(): IFunction<unknown, boolean>;

  get toDateFrom(): IFunction<unknown, Date>;

  get useNullInsteadOfDefaulValues(): boolean;

}


export abstract class AbstractConfig implements IConfig {
  private _includeStats: boolean | null = null;
  private _includeDefaultValues: boolean | null = null;
  private _defaultValues: unknown[] = [];
  private _includeEmptyArrays: boolean | null = null;
  private _includeEmptyObjects: boolean | null = null;
  private _includeNullValues: boolean | null = null;
  private _includeUndefinedValues: boolean | null = null;
  private _includeDefaultBoolean: boolean | null = null;
  private _includeDefaultNumber: boolean | null = null;
  private _includeDefaultString: boolean | null = null;
  private _includeUserDefinedDefaultValues: boolean | null = null;
  private _strategyForUnknownProperty: StrategyForUnknownProperty | null = null;
  protected _converterMap: Record<string, IFunction<unknown, unknown>>  = {
    't@b_': FunctionUtil.IDENTITY,
    't@d_': FunctionUtil.IDENTITY
  };

  get includeDefaultValues(): boolean {
    return this._includeDefaultValues as boolean;
  }

  set includeDefaultValues(val: boolean) {
    this._includeDefaultValues = val;
  }

  get includeStats(): boolean {
    return this._includeStats as boolean;
  }

  set includeStats(val: boolean) {
    this._includeStats = val;
  }

  get defaultValues(): unknown[] {
    return this._defaultValues;
  }

  get includeEmptyArrays(): boolean {
    return this._includeEmptyArrays as boolean;
  }

  set includeEmptyArrays(val: boolean) {
    this._includeEmptyArrays = val;
  }

  get includeEmptyObjects(): boolean {
    return this._includeEmptyObjects as boolean;
  }

  set includeEmptyObjects(val: boolean) {
    this._includeEmptyObjects = val;
  }

  get includeNullValues(): boolean {
    return this._includeNullValues as boolean;
  }

  set includeNullValues(val: boolean) {
    this._includeNullValues = val;
  }

  get includeUndefinedValues(): boolean {
    return this._includeUndefinedValues as boolean;
  }

  set includeUndefinedValues(val: boolean) {
    this._includeUndefinedValues = val;
  }

  get includeDefaultBoolean(): boolean {
    return this._includeDefaultBoolean as boolean;
  }

  set includeDefaultBoolean(val: boolean) {
    this._includeDefaultBoolean = val;
  }

  get includeDefaultNumber(): boolean {
    return this._includeDefaultNumber as boolean;
  }

  set includeDefaultNumber(val: boolean) {
    this._includeDefaultNumber = val;
  }

  get includeDefaultString(): boolean {
    return this._includeDefaultString as boolean;
  }

  set includeDefaultString(val: boolean) {
    this._includeDefaultString = val;
  }

  get includeUserDefinedDefaultValues(): boolean {
    return this._includeUserDefinedDefaultValues as boolean;
  }

  set includeUserDefinedDefaultValues(val: boolean) {
    this._includeUserDefinedDefaultValues = val;
  }

  get strategyForUnknownProperty(): StrategyForUnknownProperty {
    return this._strategyForUnknownProperty as StrategyForUnknownProperty;
  }

  set strategyForUnknownProperty(val: StrategyForUnknownProperty) {
    this._strategyForUnknownProperty = val;
  }

  get converterMap(): Record<string, IFunction<unknown, unknown>> {
    return this._converterMap;
  }

}



export class SerializationConfig extends AbstractConfig implements ISerializationConfig {
  protected _booleanConverter: IFunction<boolean, unknown> = FunctionUtil.IDENTITY;
  protected _dateConverter: IFunction<Date, unknown> = FunctionUtil.IDENTITY;

  get fromBooleanTo(): IFunction<boolean, unknown> {
    return this._booleanConverter;
  }

  set fromBooleanTo(val: IFunction<boolean, unknown>) {
    this._booleanConverter = val;
    this._converterMap['t@b_'] = val;
  }

  get fromDateTo(): IFunction<Date, unknown> {
    return this._dateConverter;
  }

  set fromDateTo(val: IFunction<Date, unknown>) {
    this._dateConverter = val;
    this._converterMap['t@d_'] = val;
  }

}

export class DeserializationConfig extends AbstractConfig implements IDeserializationConfig {
  protected _booleanConverter: IFunction<unknown, boolean> = FunctionUtil.IDENTITY;
  protected _dateConverter: IFunction<unknown, Date> = FunctionUtil.IDENTITY;

  private _useNullInsteadOfDefaulValues = false;

  get toBooleanFrom(): IFunction<unknown, boolean> {
    return this._booleanConverter;
  }

  set toBooleanFrom(val: IFunction<unknown, boolean>) {
    this._booleanConverter = val;
    this._converterMap['t@b_'] = val;
  }

  get toDateFrom(): IFunction<unknown, Date> {
    return this._dateConverter;
  }

  set toDateFrom(val: IFunction<unknown, Date>) {
    this._dateConverter = val;
    this._converterMap['t@d_'] = val;
  }


  public get useNullInsteadOfDefaulValues(): boolean {
    return this._useNullInsteadOfDefaulValues;
  }

  public set useNullInsteadOfDefaulValues(v: boolean) {
    this._useNullInsteadOfDefaulValues = v;
  }



}

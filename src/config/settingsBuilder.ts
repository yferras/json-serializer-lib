import { IBuilder } from '../commons';
import {IFunction} from '../func';
import {AbstractConfig, DeserializationConfig, SerializationConfig, IConfig} from './configuration';
import {StrategyForUnknownProperty} from './enums';


/**
 * Interface to define settings builders.
 */
interface ISettingsBuilder<C extends IConfig, B extends ISettingsBuilder<C, B>> extends IBuilder<C> {
  /**
   * Setting to include (or not) the stats.
   */
  includeStats(answer: boolean): B;

  /**
   * Setting to include (or not) the default values.
   */
  includeDefaultValues(answer: boolean): B;

  /**
   * Setting to include (or not) the lists that are empty.
   */
  includeEmptyArrays(answer: boolean): B;

  /**
   * Setting to include (or not) the strings that are empty.
   */
  includeDefaultString(answer: boolean): B;

  /**
   * Setting to include (or not) the null values.
   */
  includeNullValues(answer: boolean): B;

  /**
   * Setting to include (or not) the undefined values.
   */
  includeUndefinedValues(answer: boolean): B;

  /**
   * Setting to include (or not) the empty objects.
   */
  includeEmptyObjects(answer: boolean): B;

  /**
   * Setting to include (or not) the false values.
   */
  includeDefaultBoolean(answer: boolean): B;

  /**
   * Setting to include (or not) the zero values.
   */
  includeDefaultNumber(answer: boolean): B;

  /**
   * Setting to include (or not) the user defined default values.
   */
  includeUserDefinedDefaultValues(answer: boolean): B;

  /**
   * Setting to define the strategy to process the unknown properties.
  */
  strategyForUnknownProperty(strategy: StrategyForUnknownProperty): B;

}

export interface ISerializationSettingsBuilder extends ISettingsBuilder<SerializationConfig, ISerializationSettingsBuilder> {

  /**
   * Setting to define a boolean converter.
   */
  setBooleanConverter(converter: IFunction<boolean, unknown>): ISerializationSettingsBuilder;

  /**
   * Setting to define a Date converter.
   */
  setDateConverter(converter: IFunction<Date, unknown>): ISerializationSettingsBuilder;

}

export interface IDeserializationSettingsBuilder extends ISettingsBuilder<DeserializationConfig, IDeserializationSettingsBuilder> {

  /**
   * Setting to define a boolean converter.
   */
  setBooleanConverter(converter: IFunction<unknown, boolean>): IDeserializationSettingsBuilder;

  /**
   * Setting to define a Date converter.
   */
  setDateConverter(converter: IFunction<unknown, Date>): IDeserializationSettingsBuilder;

  /**
   * Setting to use null values instead of default values.
   */
   useNullInsteadOfDefaulValues(answer: boolean): IDeserializationSettingsBuilder;

}



class SettingsBuilder<C extends AbstractConfig> implements ISettingsBuilder<C, SettingsBuilder<C>>   {

    constructor(private _config: C) {
    }

    get config(): C {
      return this._config;
    }

    build(): C {
      return this._config;
    }

    includeStats(answer: boolean): SettingsBuilder<C> {
      this._config.includeStats = answer;
      return this;
    }

    includeDefaultValues(answer: boolean): SettingsBuilder<C> {
      this._config.includeDefaultValues = answer;
      this._config.includeEmptyArrays = answer;
      this._config.includeEmptyObjects = answer;
      this._config.includeDefaultString = answer;
      this._config.includeDefaultBoolean = answer;
      this._config.includeNullValues = answer;
      this._config.includeUndefinedValues = answer;
      this._config.includeDefaultNumber = answer;
      this._config.includeUserDefinedDefaultValues = answer;
      return this;
    }

    includeEmptyArrays(answer: boolean): SettingsBuilder<C> {
      this._config.includeEmptyArrays = answer;
      return this;
    }

    includeNullValues(answer: boolean): SettingsBuilder<C> {
      this._config.includeNullValues = answer;
      return this;
    }

    includeUndefinedValues(answer: boolean): SettingsBuilder<C> {
      this._config.includeUndefinedValues = answer;
      return this;
    }

    includeEmptyObjects(answer: boolean): SettingsBuilder<C> {
      this._config.includeEmptyObjects = answer;
      return this;
    }

    includeDefaultBoolean(answer: boolean): SettingsBuilder<C> {
      this._config.includeDefaultBoolean = answer;
      return this;
    }

    includeDefaultString(answer: boolean): SettingsBuilder<C> {
      this._config.includeDefaultString = answer;
      return this;
    }

    includeDefaultNumber(answer: boolean): SettingsBuilder<C> {
      this._config.includeDefaultNumber = answer;
      return this;
    }

    includeUserDefinedDefaultValues(answer: boolean): SettingsBuilder<C> {
      this._config.includeUserDefinedDefaultValues = answer;
      return this;
    }

    strategyForUnknownProperty(strategy: StrategyForUnknownProperty): SettingsBuilder<C> {
      this._config.strategyForUnknownProperty = strategy;
      return this;
    }

}


export class DeserializationSettingsBuilder implements IDeserializationSettingsBuilder {

  public static readonly DEFAULT = new DeserializationSettingsBuilder().build();

  private _settingsBuilder: SettingsBuilder<DeserializationConfig> = new SettingsBuilder(new DeserializationConfig());

  constructor() {
    this._settingsBuilder.includeDefaultValues(true)
    .strategyForUnknownProperty(StrategyForUnknownProperty.KEEP)
    .includeStats(false);
  }

  setBooleanConverter(converter: IFunction<unknown, boolean>): IDeserializationSettingsBuilder {
    this._settingsBuilder.config.toBooleanFrom = converter;
    return this;
  }
  useNullInsteadOfDefaulValues(answer: boolean): IDeserializationSettingsBuilder {
    this._settingsBuilder.config.useNullInsteadOfDefaulValues = answer;
      return this;
  }
  setDateConverter(converter: IFunction<unknown, Date>): IDeserializationSettingsBuilder {
    this._settingsBuilder.config.toDateFrom = converter;
    return this;
  }
  includeStats(answer: boolean): IDeserializationSettingsBuilder {
    this._settingsBuilder.includeStats(answer);
    return this;
  }
  includeDefaultValues(answer: boolean): IDeserializationSettingsBuilder {
    this._settingsBuilder.includeDefaultValues(answer);
    return this;
  }
  includeEmptyArrays(answer: boolean): IDeserializationSettingsBuilder {
    this._settingsBuilder.includeEmptyArrays(answer);
    return this;
  }
  includeDefaultString(answer: boolean): IDeserializationSettingsBuilder {
    this._settingsBuilder.includeDefaultString(answer);
    return this;
  }
  includeNullValues(answer: boolean): IDeserializationSettingsBuilder {
    this._settingsBuilder.includeNullValues(answer);
    return this;
  }
  includeUndefinedValues(answer: boolean): IDeserializationSettingsBuilder {
    this._settingsBuilder.includeUndefinedValues(answer);
    return this;
  }
  includeEmptyObjects(answer: boolean): IDeserializationSettingsBuilder {
    this._settingsBuilder.includeEmptyObjects(answer);
    return this;
  }
  includeDefaultBoolean(answer: boolean): IDeserializationSettingsBuilder {
    this._settingsBuilder.includeDefaultBoolean(answer);
    return this;
  }
  includeDefaultNumber(answer: boolean): IDeserializationSettingsBuilder {
    this._settingsBuilder.includeDefaultNumber(answer);
    return this;
  }
  includeUserDefinedDefaultValues(answer: boolean): IDeserializationSettingsBuilder {
    this._settingsBuilder.includeUserDefinedDefaultValues(answer);
    return this;
  }
  strategyForUnknownProperty(strategy: StrategyForUnknownProperty): IDeserializationSettingsBuilder {
    this._settingsBuilder.strategyForUnknownProperty(strategy);
    return this;
  }

  build(): DeserializationConfig {
    return this._settingsBuilder.build();
  }

}


export class SerializationSettingsBuilder implements ISerializationSettingsBuilder {

  public static readonly DEFAULT = new SerializationSettingsBuilder().build();

  private _settingsBuilder: SettingsBuilder<SerializationConfig> = new SettingsBuilder(new SerializationConfig());

  constructor() {
    this._settingsBuilder.includeDefaultValues(false)
    .strategyForUnknownProperty(StrategyForUnknownProperty.IGNORE)
    .includeStats(false);
  }

  setBooleanConverter(converter: IFunction<unknown, boolean>): ISerializationSettingsBuilder {
    this._settingsBuilder.config.fromBooleanTo = converter;
    return this;
  }
  setDateConverter(converter: IFunction<unknown, Date>): ISerializationSettingsBuilder {
    this._settingsBuilder.config.fromDateTo = converter;
    return this;
  }
  includeStats(answer: boolean): ISerializationSettingsBuilder {
    this._settingsBuilder.includeStats(answer);
    return this;
  }
  includeDefaultValues(answer: boolean): ISerializationSettingsBuilder {
    this._settingsBuilder.includeDefaultValues(answer);
    return this;
  }
  includeEmptyArrays(answer: boolean): ISerializationSettingsBuilder {
    this._settingsBuilder.includeEmptyArrays(answer);
    return this;
  }
  includeDefaultString(answer: boolean): ISerializationSettingsBuilder {
    this._settingsBuilder.includeDefaultString(answer);
    return this;
  }
  includeNullValues(answer: boolean): ISerializationSettingsBuilder {
    this._settingsBuilder.includeNullValues(answer);
    return this;
  }
  includeUndefinedValues(answer: boolean): ISerializationSettingsBuilder {
    this._settingsBuilder.includeUndefinedValues(answer);
    return this;
  }
  includeEmptyObjects(answer: boolean): ISerializationSettingsBuilder {
    this._settingsBuilder.includeEmptyObjects(answer);
    return this;
  }
  includeDefaultBoolean(answer: boolean): ISerializationSettingsBuilder {
    this._settingsBuilder.includeDefaultBoolean(answer);
    return this;
  }
  includeDefaultNumber(answer: boolean): ISerializationSettingsBuilder {
    this._settingsBuilder.includeDefaultNumber(answer);
    return this;
  }
  includeUserDefinedDefaultValues(answer: boolean): ISerializationSettingsBuilder {
    this._settingsBuilder.includeUserDefinedDefaultValues(answer);
    return this;
  }
  strategyForUnknownProperty(strategy: StrategyForUnknownProperty): ISerializationSettingsBuilder {
    this._settingsBuilder.strategyForUnknownProperty(strategy);
    return this;
  }

  build(): SerializationConfig {
    return this._settingsBuilder.build();
  }

}




import * as commons from './commons';
import { INativePropSchema, IObjectPropSchema, ISchema, Schema } from './schema';
import {
  ConfigurationUtil as ConfUtil,
  IConfig,
  IDeserializationConfig,
  ISerializationConfig,
  StrategyForUnknownProperty as S4UnkProp
} from './config';
import { InputType, DefaultValues, FunctionUtil, IFunction, IBiFunction } from './func';
import {TypeDescriptor} from './schema/enums';
import {Logger} from './commons';



type native = number | string | boolean | Date;
type record = Record<string | number | symbol, unknown>;


/**
 * enum to define all the actions.
 */
enum Action {
  EXCLUDE, INCLUDE
}

class ProcessOutput {

  /**
   * Constructor.
   *
   * @param _result is any value (by default is undefined).
   * @param _action flag to keep or remove the result (by default is INCLUDE).
   * @param _isOk flag to determine if the result needs more processing (false by default) or not (true).
   */
  constructor(
    private _result: unknown = undefined,
    private _action: Action = Action.INCLUDE,
    private _isOk: boolean = false,
  ) {

  }

  /**
   * Gets the action.
   *
   * @returns the action.
   */
  public get action(): Action {
    return this._action;
  }

  /**
   * Gets the action.
   *
   * @returns the action.
   */
  public set action(v: Action) {
    this._action = v;
  }

  /**
   * Gets the result.
   *
   * @returns any value.
   */
  public get result(): unknown {
    return this._result;
  }

  /**
   * Gets if this result is OK.
   *
   * @returns true if and only if the resul don't need more processing; otherwise returns false.
   */
  public get isOk(): boolean {
    return this._isOk;
  }

}

abstract class AbstractModel {

  /**
   * Gets a proxified function.
   *
   * @param input the input instance of {@link commons.Input}
   * @param config instance of {@link commons.Input}
   * @param func function to proxify.
   *
   * @returns a function.
   */
  protected createProxyFunction(input: commons.Input, config: IConfig, func: (i: unknown, j: unknown, c: IConfig) => unknown ):
    () => {json: unknown, stats?: commons.Stats}
  {
    if (config.includeStats) {
      return () => {
        let runtime = new Date().getTime();
        const result = func(input.json, input.schema, config);
        runtime = new Date().getTime() - runtime;
        const inSize = JSON.stringify(input.json).length;
        const outSize = JSON.stringify(result).length;
        const stats = {
          inputSize: inSize,
          outputSize: outSize,
          ratio: outSize / inSize,
          runtime: runtime
        };

        return {
          json: result,
          stats: stats
        }

      };
    }
    return () => {
      return {
        json: func(input.json, input.schema, config)
      }
    };
  }

  /**
   * Checks if the value is keeped or removed.
   */
  protected check(value: unknown, schema: ISchema | null, config: IConfig): ProcessOutput {
    let userDefaultValues: unknown[] | null = null;
    let keep = false;
    if (schema && (schema.hasDefVal || schema.keepDef)) {
      userDefaultValues = [ schema.defVal ];
      keep = schema.keepDef;
    }
    let isOk = false;
    let include = false;
    switch (FunctionUtil.DEFAULT_VALUE_CLASSIFIER.apply(value, userDefaultValues)) {
      case DefaultValues.EMPTY_ARRAY:
        include = config.includeEmptyArrays;
      break;
      case DefaultValues.EMPTY_OBJ: {
        if (value instanceof Date) {
          include = true;
        } else {
          include = config.includeEmptyObjects;
        }
      }
      break;
      case DefaultValues.STRING:
        include = config.includeDefaultString;
      break;
      case DefaultValues.BOOLEAN:
        include = config.includeDefaultBoolean;
      break;
      case DefaultValues.NULL: {
        include = config.includeNullValues || keep;
        isOk = true; // No more post-process.
      }
      break;
      case DefaultValues.UNDEFINED: {
        include = config.includeUndefinedValues || keep;
        isOk = true; // No more post-process.
      }
      break;
      case DefaultValues.NUMBER:
        include = config.includeDefaultNumber;
      break;
      case DefaultValues.USER_DEFINED: {
        include = config.includeUserDefinedDefaultValues || keep;
        isOk = true; // No more post-process.
      }
      break;
      default: include = true;
    }

    return new ProcessOutput(value, include ? Action.INCLUDE : Action.EXCLUDE, isOk);
  }

  /**
   * Process native types.
   *
   * @param typeDescriptor the type descriptor (number, string, date, boolean).
   * @param value input value.
   * @param schema schema to process the input.
   * @param config instance of {@link IConfig}.
   */
  private native(value: native, schema: INativePropSchema, config: IConfig): ProcessOutput {
    const out = this.check(value, schema, config);
    if (out.action === Action.EXCLUDE || out.isOk) {
      return out;
    }
    return this.processNative(value, schema, config);
  }

  /**
   * Process native types (abstracttion).
   *
   * @param value input value.
   * @param schema schema to process the input.
   * @param config instance of {@link IConfig}.
   */
  protected abstract processNative(value: native, schema: INativePropSchema, config: IConfig): ProcessOutput;

  protected abstract postProcess(val: record, schema: IObjectPropSchema, config: IConfig): ProcessOutput;

  /**
   * Process arrays.
   *
   * @param input array.
   * @param schema schema to process the input.
   * @param config instance of {@link IConfig}.
   */
  protected processArray(input: unknown[], schema: ISchema, config: IConfig): ProcessOutput {


    const out = this.check(input, schema, config);
    if (out.action === Action.EXCLUDE || out.isOk) {
      return out;
    }

    const array: unknown[] = [];
    for (const item of input) {

      // Respect the default values
      if (FunctionUtil.DEFAULT_VALUE_CLASSIFIER.apply(item) !== DefaultValues.NOT_A_DEFAULT_VALUE) {
        array.push(item);
        continue;
      }

      let out = new ProcessOutput(null, Action.EXCLUDE);
      const inputType = FunctionUtil.TYPE_CLASSIFIER.apply(item);
      switch (inputType) {
        case InputType.ARRAY:
          out = this.processArray(item as unknown[], schema, config);
        break;
        case InputType.OBJECT:
          out = this.processObject(item as record, (schema as IObjectPropSchema), config);
        break;
        case InputType.NATIVE:
          out = this.native(item as native, (schema as INativePropSchema), config);
        break;
        default:
          Logger.instance.warn(() => `${inputType} is not valid to process an array.`);
      }
      if (out.action === Action.INCLUDE) {
        array.push(out.result);
      }
    }
    return new ProcessOutput(array, Action.INCLUDE, false);
  }

  /**
   * Process object.
   *
   * @param obj object.
   * @param schema schema to process the input.
   * @param config instance of {@link IConfig}.
   */
  protected processObject(obj: record, schema: IObjectPropSchema, config: IConfig): ProcessOutput {

    const out = schema.parent == null ?
      new ProcessOutput(obj, Action.INCLUDE) :  // Skip the check.
      this.check(obj, schema, config);
    if (out.action === Action.EXCLUDE || out.isOk) {
      return out;
    }

    const newObj: record = {};
    const warnings: string[] = [];

    for (const property in obj) { // Iterate by all properties.
      const sch = schema.map.get(property);

      if (!sch) {
        // If the property is not defined.
        warnings.push(`Property: ''${property}'' is not defined in the schema.`)
        switch (config.strategyForUnknownProperty) {
          case S4UnkProp.IGNORE:
            break;
          case S4UnkProp.KEEP: {
            const out1 = this.check(obj[property], null, config);
            if (out1.action === Action.INCLUDE) {
              newObj[property] = out1.result;
            }
          }
          break;
          case S4UnkProp.THROW_ERROR:
            throw new Error(warnings[warnings.length - 1]);
          default:
            Logger.instance.warn(() => `Is not a valid strategy.`);
          break;
        }
        continue;
      }

      const value = obj[property];
      let out: ProcessOutput = new ProcessOutput(value, Action.EXCLUDE);
      const inputType = FunctionUtil.TYPE_CLASSIFIER.apply(value);
      switch (inputType) {
        case InputType.ARRAY:
          out = this.processArray(value as unknown[], sch, config);
        break;
        case InputType.OBJECT:
          out = this.processObject(value as record, (sch as IObjectPropSchema), config);
        break;
        case InputType.NATIVE:
          out = this.native(value as native, (sch as INativePropSchema), config);
        break;
        default:
          Logger.instance.warn(() => `${inputType} is not valid to process an object.`);
        break;
      }

      if (out.action === Action.INCLUDE) {
        newObj[sch.propNewName] = out.result;
      }
    }

    warnings.forEach(wm => Logger.instance.warn(() => wm));
    return this.postProcess(newObj, schema, config);
  }

  /**
   * Process the input object.
   *
   * @param input object or array to process.
   * @param jsonSchema schema to process the input (JSON format).
   * @param config instance of {@link IConfig}.
   *
   * @returns new processed object.
   */
  protected process(input: unknown, jsonSchema: unknown, config: IConfig): unknown {

    if (FunctionUtil.TYPE_CLASSIFIER.apply(jsonSchema) !== InputType.OBJECT) {
      return input;
    }
    const schema = Schema.getSchema(jsonSchema);
    switch (FunctionUtil.TYPE_CLASSIFIER.apply(input)) {
      case InputType.ARRAY:
        return this.processArray(input as unknown[], schema, config).result;
      case InputType.OBJECT:
        return this.processObject(input as record, schema, config).result;
      case InputType.NATIVE:
        return input;
      default:
        return {};
    }
  }

}

/**
 * Deserializer class.
 */
class Deserializer
extends AbstractModel
implements IBiFunction<commons.Input, IConfig, commons.IDeserializedOutput<unknown>> {

  constructor() {
    super();
  }

  /**
   * Process native types.
   *
   * @param value input value.
   * @param schema schema to process the input.
   */
  protected processNative(value: native, schema: INativePropSchema, config: IConfig): ProcessOutput {
    // Get the default function by the type of the native
    // to transform the native value.
    // By default is IDENTITY function.
    let retFunc: IFunction<unknown, unknown> =
      schema.typeDesc.toString() in config.converterMap ?
      config.converterMap[schema.typeDesc.toString()]:
      FunctionUtil.IDENTITY;

    let arg = value;
    retFunc = schema.hasConv && schema.desFn || retFunc;


    if (schema.hasDefVal) {
      arg = value !== undefined ? value : schema.defVal as native;
    }

    if (value === undefined) {
      return new ProcessOutput(arg, Action.INCLUDE);
    }

    return new ProcessOutput(retFunc.apply(arg), Action.INCLUDE, false);
  }

  protected postProcess(obj: record, schema: IObjectPropSchema, config: IConfig): ProcessOutput {
    if (schema.typeDesc === TypeDescriptor.ARRAY && FunctionUtil.DEEP_EQUALS.apply({}, obj)) {
      const action = config.includeEmptyArrays ? Action.INCLUDE : Action.EXCLUDE;
      return new ProcessOutput([], action);
    }
    schema.map.forEach((subSchema: ISchema) => {

      if (subSchema.propNewName in obj) {
        return;
      }

      if (subSchema.hasDefVal) {
        const out = this.check(subSchema.defVal, subSchema, config);
        if (out.action === Action.INCLUDE) {
          obj[subSchema.propNewName] = out.result;
        }
        return;
      }

      if (Object.prototype.hasOwnProperty.call(subSchema, 'map')) {
        const objPropSch = (subSchema as IObjectPropSchema);
        const out = this.postProcess({}, objPropSch, config);
        if (out.action === Action.INCLUDE || out.isOk) {
          obj[subSchema.propNewName] = out.result;
        }
        return;
      }

      const natPropSch = (subSchema as INativePropSchema);
      let defVal = null;
      const dConf = (config as IDeserializationConfig);
      if (natPropSch.hasDefVal) {
        defVal = natPropSch.defVal;
      } else if (!dConf.useNullInsteadOfDefaulValues) {
        defVal = Schema.getDefaultValueByType(natPropSch.typeDesc);
      }
      const out = this.check(defVal, natPropSch, config);
      if (out.action === Action.INCLUDE) {
        obj[natPropSch.propNewName] = out.result;
      }
    });

    let action = Action.INCLUDE;
    if (!config.includeEmptyObjects && FunctionUtil.DEEP_EQUALS.apply({}, obj)) {
      action = Action.EXCLUDE;
    }

    return new ProcessOutput(obj, action);
  }

  apply(input: commons.Input, altConfig?: IConfig): commons.IDeserializedOutput<unknown> {
    const result = this.createProxyFunction(input, altConfig || ConfUtil.globalDesConfig, super.process.bind(this))();
    return new commons.DeserializedOutput(result.json, result.stats);
  }

}

class Serializer
extends AbstractModel
implements IBiFunction<commons.Input, IConfig, commons.ISerializedOutput<unknown>>{

  constructor() {
    super();
  }

  /**
   * Process native types.
   *
   * @param value input value.
   * @param schema schema to process the input.
   * @param config instance of {@link IConfig}.
   */
  protected processNative(value: native, schema: INativePropSchema, config: IConfig): ProcessOutput {

    // Get the default function by the type of the native
    // to transform the native value.
    // By default is IDENTITY function.
    let retFunc: IFunction<unknown, unknown> =
      schema.typeDesc.toString() in config.converterMap ?
      config.converterMap[schema.typeDesc.toString()]:
      FunctionUtil.IDENTITY;

    let arg = value;
    retFunc = schema.hasConv && schema.serFn || retFunc;
    let action = Action.INCLUDE;

    if (schema.hasDefVal) { // User defined default value.
      arg = value !== undefined ? value : schema.defVal as native;
      if (!config.includeDefaultValues && value === schema.defVal) {
        action = schema.keepDef ? Action.INCLUDE : Action.EXCLUDE;
      }
    }
    return new ProcessOutput(retFunc.apply(arg), action, false);
  }

  protected postProcess(val: record, schema: ISchema, config: IConfig): ProcessOutput {
    let action = Action.INCLUDE;
    if (!config.includeEmptyObjects && FunctionUtil.DEEP_EQUALS.apply({}, val)) {
      action = Action.EXCLUDE;
    }
    return new ProcessOutput(val, action);
  }

  apply(input: commons.Input, altConfig?: IConfig): commons.ISerializedOutput<unknown> {
    const result = this.createProxyFunction(input, altConfig || ConfUtil.globalSerConfig, super.process.bind(this))();
    return new commons.SerializedOutput(result.json, result.stats);
  }

}

export class Model {
  private static readonly DESERIALIZER_INSTANCE: IBiFunction<commons.Input, IConfig | null, commons.IDeserializedOutput<unknown>> = new Deserializer();
  private static readonly SERIALIZER_INSTANCE: IBiFunction<commons.Input, IConfig | null, commons.ISerializedOutput<unknown>> = new Serializer();


  public static serialize(obj: unknown, schema: unknown, altConfig?: ISerializationConfig): commons.ISerializedOutput<unknown> {

    const input: commons.Input = {
      json: obj,
      schema: schema,
    };
    return Model.SERIALIZER_INSTANCE.apply(input, altConfig);
  }

  public static deserialize(obj: unknown, schema: unknown, altConfig?: IDeserializationConfig): commons.IDeserializedOutput<unknown> {

    const input: commons.Input = {
      json: obj,
      schema: Schema.invert(schema as Record<string, unknown>),
    };
    return Model.DESERIALIZER_INSTANCE.apply(input, altConfig);
  }

}

export enum InputType {
  /**
   * For arrays.
   * = 0
   */
  ARRAY,
  /**
   * Only for: 'string' | 'number' | 'boolean' | 'undefined' | 'null' | 'Date'.
   * = 1
   */
  NATIVE,
  /**
   * Only for: 'object'
   * = 2
   */
  OBJECT,
  /**
   * Any else.
   * = 3
   */
  UNKNOWN
}


export const enum DefaultValues {
  USER_DEFINED = -2,
  NOT_A_DEFAULT_VALUE = -1,
  UNDEFINED,
  NUMBER,
  BOOLEAN,
  STRING,
  NULL,
  EMPTY_OBJ,
  EMPTY_ARRAY,
}



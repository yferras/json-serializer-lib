import {IFunction} from "../func";

class UpperCaseModifier implements IFunction<string, string> {

  apply(arg: string): string {
    return arg.toUpperCase();
  }

}

class LowerCaseModifier implements IFunction<string, string> {

  apply(arg: string): string {
    return arg.toLowerCase();
  }

}
/**
 * String modifiers.
 */
export const StringModifier = {
  /**
   * The input string will be modified to the upper case string.
   *
   * Example:
   *    StringModifier.UPPER_CASE.apply('abcd'); // Will be modified like: 'ABCD'.
   */
  UPPER_CASE: new UpperCaseModifier(),
  /**
   * The input string will be modified to the lower case string.
   *
   * Example:
   *    StringModifier.LOWER_CASE.apply('ABCD'); // Will be modified like: 'abcd'.
   */
  LOWER_CASE: new LowerCaseModifier()
}



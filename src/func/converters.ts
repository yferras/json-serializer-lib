import {Logger} from "../commons";
import {IFunction} from "./functions";


abstract class AbstractConverterBasedOnMap<A, R> implements IFunction<A, R> {

  protected _map: Record<string, R>;

  protected constructor(map: Record<string, R>) {
    this.validate(map);
    this._map = map;
  }

  protected validate(map: Record<string, R>): void {
    if (!map) {
      throw new Error('"map" must be defined.');
    }
  }

  protected abstract getValueFromKey(key: A): R;

  apply(arg: A): R {
    return this.getValueFromKey(arg);
  }

  /**
   * @returns a bound function of apply function.
   */
  get func() : CallableFunction {
    return this.apply.bind(this);
  }

}

class ConvertFromBoolean extends AbstractConverterBasedOnMap<boolean, string | number> {

  constructor(map: Record<string, string | number>) {
    super(map);
  }

  protected validate(map:  Record<string, string | number>): void {
    super.validate(map);
    if (!('true' in map) || !('false' in map)) {
      throw new Error('Property "true" and property "false", both must be defined.');
    }
  }

  protected getValueFromKey(key: boolean): string | number {
    const str = key ? 'true' : 'false';
    return this._map[str];
  }

  public static toCustom(map: Record<string, string | number>): IFunction<boolean, unknown> {
    return new ConvertFromBoolean(map);
  }


}

class ConvertToBoolean extends AbstractConverterBasedOnMap<string | number, boolean> {

  constructor(map: Record<string | number, boolean>) {
    super(map);
  }

  protected validate(map: Record<string | number, boolean>) {
    super.validate(map);
    let c = 0;
    for (const prop in map) {
      if (map[prop] === true || map[prop] === false) {
        c++;
        continue;
      }
      delete map[prop];
    }
    if (c < 2) {
      throw new Error('The map must contain a least two properties with the values: "true" and "false". Example: {"a": true, "b": false}.');
    }

    if (c > 2) {
      Logger.instance.warn(() => 'More than 3 properties are defined.');
    }
  }

  protected getValueFromKey(key: string | number): boolean {
    if (Object.prototype.hasOwnProperty.call(this._map, key)) {
      return this._map[key];
    }
    throw new Error(`There is not property: ${key} defined in _map`);
  }

  public static toCustom(map: Record<string | number, boolean>): IFunction<string | number, boolean | undefined> {
    return new ConvertToBoolean(map);
  }
}

class ConvertFromDate implements IFunction<Date, number> {

  apply(arg: Date): number {
    return arg.getTime();
  }

}

class ConvertToDate implements IFunction<number, Date> {

  apply(arg: number): Date {
    return new Date(arg);
  }

}

export class Convert {
  public static readonly FromBoolean = {
    /**
     * Instance of {@link IFunction} that converts:
     *    true to 1
     *    false to 0
     */
    To_0_Or_1: new ConvertFromBoolean({'true': 1, 'false': 0}),
    /**
     * Instance of {@link IFunction} that converts:
     *    true to 'T'
     *    false to 'F'
     */
    To_F_Or_T: new ConvertFromBoolean({'true': 'T', 'false': 'F'}),
    /**
     * Instance of {@link IFunction} that converts:
     *    true to 'Y'
     *    false to 'N'
     */
    To_N_Or_Y: new ConvertFromBoolean({'true': 'Y', 'false': 'N'}),
    /**
     * Custom converter.
     *
     * Example:
     * const convToBool = Convert.Boolean.TO_CUSTOM(
     *    {
     *      'true': 'A',
     *      'false': 'B'
     *    }
     * );
     *
     * convToBool.apply(true); // Returns 'A'.
     * convToBool.apply(false); // Returns 'B'.
     */
    ToCustom: ConvertFromBoolean.toCustom
  };
  public static readonly ToBoolean = {
    /**
     * Instance of {@link IFunction} that converts:
     *    1 to true
     *    0 to false
     */
    From_0_Or_1: new ConvertToBoolean({'1': true, '0': false}),
    /**
     * Instance of {@link IFunction} that converts:
     *    'T' to true
     *    'F' to false
     */
    From_F_Or_T: new ConvertToBoolean({'T': true, 'F': false}),
    /**
     * Instance of {@link IFunction} that converts:
     *    'Y' to true
     *    'N' to false
     */
    From_N_Or_Y: new ConvertToBoolean({'Y': true, 'N': false}),
    /**
     * Custom converter.
     *
     * Example:
     * const convToBool = Convert.Boolean.FROM_CUSTOM(
     *    {
     *      'A': true,
     *      'B': false
     *    }
     * );
     *
     * convToBool.apply('A'); // Returns true.
     * convToBool.apply('B'); // Returns false.
     */
    FromCustom: ConvertToBoolean.toCustom
  };

  public static readonly FromDate = {
    /**
     * Converts from Date to time in milliseconds.
     */
    ToTimeInMillis: new ConvertFromDate()
  }

  /**
   * Converts from time in milliseconds to Date.
   */
  public static readonly ToDate = {
    FromTimeInMillis: new ConvertToDate()
  }
}


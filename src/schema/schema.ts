import {Logger} from '../commons';
import {ConfigurationUtil} from '../config';
import { InputType, FunctionUtil, IFunction } from '../func';
import {Key, TypeDescriptor} from './enums';


export interface ISchema {
  parent: IObjectPropSchema | null,
  typeDesc: TypeDescriptor;
  propInObj: string;
  propInSch: string;
  propNewName: string
  defVal: unknown;
  hasDefVal: boolean;
  keepDef: boolean;
}

export interface INativePropSchema extends ISchema {
  hasConv: boolean;
  serFn: IFunction<unknown, unknown> | null,
  desFn: IFunction<unknown, unknown> | null,
}

export interface IObjectPropSchema extends ISchema {
  map: Map<string, ISchema>,
}
/**
 * Utilities for schema.
 */
export class Schema {
  /**
   * Group 1: type descriptor
   * Group 2: property name.
   */
  public static readonly TYPE_DESCRIPTOR_REG_EXP: RegExp = /^(t@(?:a|b|o|n|s|d)_)(.+)/;

  public static parseString(str: string): [string, string] {
    const parsed = Schema.TYPE_DESCRIPTOR_REG_EXP.exec(str);
    let r: [string, string] = [TypeDescriptor.NONE.toString(), str];
    if (parsed && parsed.length == 3) {
      for (const key in TypeDescriptor) {
        if (TypeDescriptor[key] === parsed[1]) {
          r = [TypeDescriptor[key], parsed[2]];
          break;
        }
      }
    }
    Logger.instance.debug(() => `\nparseString('${str}') -> [${r.map(el =>"'" + el + "'").join(', ')}]\n`);
    return r;
  }

  /**
   * Inverts the given schema.
   *
   * @param schema the input schema.
   * @returns the inverted schema.
   */
  public static invert(schema: Record<string, unknown>): Record<string, unknown> {
    const invertedSchema: Record<string, unknown> = {};
    for (const property in schema) {
      const value = schema[property];
      const [typeDescriptor, newValue] = Schema.parseString(property);
      const inputType = FunctionUtil.TYPE_CLASSIFIER.apply(value);
      switch (inputType) {
        case InputType.NATIVE:
          invertedSchema[typeDescriptor + value] = newValue;
        break;
        case InputType.OBJECT: {
          const invSch: Record<string, unknown> = {};
          invSch[Key.PROP_NAME] = newValue;
          const objDescriptor = value as Record<string, unknown>;
          if (Key.DEFAULT_VALUE in objDescriptor) {
            invSch[Key.DEFAULT_VALUE] = objDescriptor[Key.DEFAULT_VALUE];
          }
          if (Key.KEEP_DEFAULT_VALUE in objDescriptor) {
            invSch[Key.KEEP_DEFAULT_VALUE] = objDescriptor[Key.KEEP_DEFAULT_VALUE];
          }
          if (Key.CONVERTER in objDescriptor) {
            // NOTE: the _serFunc and _desFunc are not interchanged, on purpose.
            invSch[Key.CONVERTER] = objDescriptor[Key.CONVERTER];
          }
          if (Key.SCHEMA in objDescriptor) {
            invSch[Key.SCHEMA] = Schema.invert(objDescriptor[Key.SCHEMA] as Record<string, unknown>);
          }
          invertedSchema[typeDescriptor + objDescriptor[Key.PROP_NAME]] = invSch;
        }

        break;
        default:
          Logger.instance.warn(() => `${inputType} is not valid to invert a schema.`);
        break;
      }
    }

    return invertedSchema;
  }

  /**
   * Merges one schema into another schema.
   *
   * @param src source schema.
   * @param dst destination schema.
   */
  public static merge(src: Record<string, unknown>, dst: Record<string, unknown>): void {
    for (const property in src) {

      if (property in dst && FunctionUtil.TYPE_CLASSIFIER.apply(src[property]) === InputType.OBJECT) {
        Schema.merge(src[property] as Record<string, unknown>, dst[property] as Record<string, unknown>);
      } else {
        dst[property] = src[property];
      }

    }
  }

  public static getTypeDescriptor(str: string) : TypeDescriptor {
    const [typeDescriptorStr, ] = Schema.parseString(str);
    switch (typeDescriptorStr || str) {
      case TypeDescriptor.OBJECT.toString():
        return TypeDescriptor.OBJECT;
      case TypeDescriptor.ARRAY.toString():
        return TypeDescriptor.ARRAY;
      case TypeDescriptor.STRING.toString():
        return TypeDescriptor.STRING;
      case TypeDescriptor.NUMBER.toString():
        return TypeDescriptor.NUMBER;
      case TypeDescriptor.BOOLEAN.toString():
        return TypeDescriptor.BOOLEAN;
      case TypeDescriptor.DATE.toString():
        return TypeDescriptor.DATE;
      default:
        return TypeDescriptor.NONE;
    }
  }

  public static getDefaultValueByType(typeDescriptor: TypeDescriptor): unknown {
    switch (typeDescriptor) {
      case TypeDescriptor.OBJECT:
        return {};
      case TypeDescriptor.ARRAY:
        return [];
      case TypeDescriptor.STRING:
        return ConfigurationUtil.DEFAULTS.STRING;
      case TypeDescriptor.NUMBER:
        return ConfigurationUtil.DEFAULTS.NUMBER;
      case TypeDescriptor.BOOLEAN:
        return ConfigurationUtil.DEFAULTS.BOOLEAN;
      default:
        return null;
    }
  }


  private static hasDefaultValue(schema: Record<string, unknown>): boolean {
    return schema && Key.DEFAULT_VALUE in schema;
  }

  private static getDefaultValue(schema: Record<string, unknown>): unknown {
    return Schema.hasDefaultValue(schema) ? schema[Key.DEFAULT_VALUE] : null;
  }

  private static getConverterFunction(schema: Record<string, unknown>, functionKey: string): IFunction<unknown, unknown> | null {
    if (FunctionUtil.TYPE_CLASSIFIER.apply(schema) !== InputType.OBJECT) {
      return null;
    }
    const objDescriptor = schema as Record<string, unknown>;
    if (!(Key.CONVERTER in objDescriptor)) {
      return null;
    }
    const converter = objDescriptor[Key.CONVERTER] as Record<string, unknown>;
    if (!(functionKey in converter)) {
      return null;
    }
    return {
      apply: converter[functionKey] as (a:unknown) => unknown
    };
  }

  public static printPath(schema: ISchema): string {
    let path = schema.propInObj;
    let parent = schema.parent;
    while (parent !== null) {
      path = parent.propInObj + '.' + path;
      parent = parent.parent;
    }
    return path;
  }

  public static getSchema(jsonSch: unknown): IObjectPropSchema {
    const schema: IObjectPropSchema = {
      parent: null,
      typeDesc: TypeDescriptor.OBJECT,
      propInObj: '$',
      propInSch: '',
      propNewName: '',
      map: new Map<string, ISchema>(),
      hasDefVal: false,
      defVal: null,
      keepDef: false
    };

    Schema.buildSchema(jsonSch as Record<string, unknown>, schema);

    return schema;
  }

  private static buildSchema(object: Record<string, unknown>, schema: IObjectPropSchema): void {
    for (const key in object) {
      if (!Object.prototype.hasOwnProperty.call(object, key)) {
        continue;
      }
      const element = object[key];
      const typeOfElement = FunctionUtil.TYPE_CLASSIFIER.apply(element);
      if (typeOfElement === InputType.NATIVE) {
        Schema.putNativePropSchema(element as string, key, schema);
      } else if (typeOfElement === InputType.OBJECT) {
        const objDescriptor = element as Record<string, unknown>;
        if (Key.SCHEMA in objDescriptor) {
          const objectItem = Schema.putObjectPropSchema(objDescriptor, key, schema);
          Schema.buildSchema(objDescriptor[Key.SCHEMA] as Record<string, unknown>, objectItem);
        } else {
          Schema.putNativePropSchema(objDescriptor, key, schema);
        }
      } else {
        Logger.instance.warn(() => `${typeOfElement} is not valid to genetare a schema.`);
      }
    }
  }

  private static putNativePropSchema(element: string | Record<string, unknown>, key: string, schema: IObjectPropSchema): void  {
    let serFn;
    let desFn;
    let newName: string;
    let keepDef: boolean;
    let hasDefVal: boolean;
    let defVal: unknown;
    const [typeDescriptorStr, propertyName] = Schema.parseString(key);
    if (typeof element === 'string') {
      newName = element;
      serFn = null;
      desFn = null;
      keepDef = false;
      hasDefVal = false;
      defVal = null;
      // Inherite.
      if (schema.hasDefVal && FunctionUtil.TYPE_CLASSIFIER.apply(schema.defVal) === InputType.OBJECT) {
        const objDefVal = schema.defVal as Record<string, unknown>;
        if (propertyName in objDefVal) {
          defVal = objDefVal[propertyName];
        } else if (newName in objDefVal) {
          defVal = objDefVal[newName];
        }
      }
    } else {
      newName = element[Key.PROP_NAME] as string;
      serFn = Schema.getConverterFunction( element, Key.SERIALIZE_FUNCTION );
      desFn = Schema.getConverterFunction( element, Key.DESERIALIZE_FUNCTION );
      keepDef = element[Key.KEEP_DEFAULT_VALUE] === true;
      hasDefVal = Schema.hasDefaultValue(element);
      defVal = Schema.getDefaultValue(element);
    }
    schema.map.set(propertyName, {
      parent: schema,
      typeDesc: Schema.getTypeDescriptor(typeDescriptorStr),
      propInSch: key,
      propInObj: propertyName,
      propNewName: newName,
      hasDefVal: schema.hasDefVal || hasDefVal,
      defVal: defVal,
      keepDef: schema.keepDef || keepDef,
      hasConv: serFn !== null || desFn !== null,
      serFn: serFn,
      desFn: desFn
    } as INativePropSchema);
  }

  private static putObjectPropSchema(element: Record<string, unknown>, key: string, schema: IObjectPropSchema): IObjectPropSchema {
    const [typeDescriptorStr, propertyName] = Schema.parseString(key);
    const item: IObjectPropSchema = {
      parent: schema,
      typeDesc: Schema.getTypeDescriptor(typeDescriptorStr),
      propInSch: key,
      propInObj: propertyName,
      propNewName: element[Key.PROP_NAME] as string,
      map: new Map<string, ISchema>(),
      hasDefVal: Schema.hasDefaultValue(element),
      defVal: Schema.getDefaultValue(element),
      keepDef: element[Key.KEEP_DEFAULT_VALUE] === true
    };
    schema.map.set(propertyName, item);
    return item;
  }

}




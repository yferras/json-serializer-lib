import {Logger} from "../commons";
import {FunctionUtil, IBiFunction, IFunction, InputType} from "../func";
import {Key} from "./enums";
import {Schema} from "./schema";

/**
 * Abstraction class for schema generation.
 */
type record = Record<string | number | symbol, unknown>;
export abstract class AbstractSchemaGenerator
  implements IBiFunction<record | record[], IFunction<string, string>, record> {

  protected constructor() {
    /* inherit */
  }

  /**
   * Abstraction to generate the schema.
   *
   * @param input - the input to get the schema.
   * @param modifier - to modified the schema.
   */
  apply(input: record | record[], modifier: IFunction<string, string> = FunctionUtil.IDENTITY): record {
    const skeleton = this.getSkeleton(input);
    return this.getSchema(skeleton, modifier);
  }

  /**
   * Gets the schema from the skeleton.
   *
   * @param skeleton the skeleton @see AbstractSchemaGenerator.getSkeleton
   */
  private getSchema(skeleton: Record<string, unknown>, modifier: IFunction<string, string>): record {
    const propNameList: string[] = [];
    const schemas: Record<string, unknown> = {};

    for (const property in skeleton) {
      const val = skeleton[property];
      const inputType = FunctionUtil.TYPE_CLASSIFIER.apply(val);
      switch (inputType) {
        case InputType.NATIVE:
          propNameList.push(property);
        break;
        case InputType.OBJECT: {
          schemas[property] = this.getSchema(val as Record<string, unknown>, modifier);
          propNameList.push(property);
        }
        break;
        default:
          Logger.instance.warn(() => `${inputType} is not valid to build a schema.`);
        break;
      }
    }

    const result: record = {};
    if (propNameList.length > 0) {
      const newPropNameList = this.generate(propNameList.sort(), modifier);
      for (let i = 0; i < newPropNameList.length; ++i) {
        const propName = propNameList[i];
        if (!schemas[propName]) {
          result[propName] = newPropNameList[i];
          continue;
        }
        const sub: record = {};
        sub[Key.PROP_NAME] = newPropNameList[i];
        sub[Key.SCHEMA] = schemas[propName];
        result[propName] = sub;
      }

    }

    return result;
  }

  /**
   * Gets a simple representation of the input.
   *
   * @param input the source from get the skeleton.
   * @param skeleton the empty skeleton.
   *
   * @returns the skeleton.
   */
  private getSkeleton(input: record | record[], skeleton: Record<string, unknown> = {}): Record<string, unknown> {

    if (FunctionUtil.TYPE_CLASSIFIER.apply(input) === InputType.ARRAY) {
      const array = input as record[];
      for (let i = 0; i < array.length; i++) {
        if (FunctionUtil.TYPE_CLASSIFIER.apply(input[i]) === InputType.NATIVE) {
          continue;
        }
        Schema.merge(this.getSkeleton(input[i] as record, skeleton), skeleton);
      }
      return skeleton;
    }


    for (const property in input) {
      const val = input[property];
      const inputType = FunctionUtil.TYPE_CLASSIFIER.apply(val);
      switch (inputType) {
        case InputType.NATIVE: {
          if (val instanceof Date) {
            skeleton[this.getPropertyName('d', property)] = '';
          } else {
            skeleton[this.getPropertyName((typeof val)[0], property)] = '';
          }
        }
        break;
        case InputType.OBJECT:
          skeleton[this.getPropertyName('o', property)] = this.getSkeleton(val as record);
        break;
        case InputType.ARRAY: {
          const aux = this.getSkeleton(val as record[]);
          Schema.merge(skeleton[this.getPropertyName('a', property)] as Record<string, unknown>, aux);
          skeleton[this.getPropertyName('a', property)] = FunctionUtil.DEEP_EQUALS.apply(aux, {}) ? '' : aux;
        }
        break;
        default:
          Logger.instance.warn(() => `${inputType} is not valid to get a skeleton.`);
        break;
      }
    }

    return skeleton;
  }

  private getPropertyName(typ: string, propertyName: string): string {
    return GeneratorUtil.INCLUDE_TYPE_DESCRIPTORS ? `t@${typ}_${propertyName}` : propertyName;
  }

  /**
   * Abstraction to generate the schema.
   *
   * @param propNameList - the list of the property names.
   * @param modifier - to modified the names.
   */
  protected abstract generate(propNameList: string[], modifier: IFunction<string, string>): string[];

}

/**
 * Generates the names of the properties with the minimum length.
 */
class MinGenerator extends AbstractSchemaGenerator {
  private static readonly SRC : string[] = [  ];

  public constructor() {
    super();
    for (let i:number = 'a'.charCodeAt(0); i <= 'z'.charCodeAt(0); i++ ) {
      MinGenerator.SRC.push(String.fromCharCode(i));
    }
  }

  generate(propNameList: string[], modifier: IFunction<string, string>): string[] {
    const result: string[] = [];
    for (let i = 0; i < propNameList.length; i++) {
      let newName = this.encode(i);
      let count = 1;
      while (result.indexOf(newName) !== -1) {
        newName = this.encode(i + count++);
      }
      result.push(modifier.apply(this.encode(i)));
    }
    return result;
  }

  private encode(num: number) : string {
    let result = '';
    let c = false;
    while (num >= MinGenerator.SRC.length) {
      result += MinGenerator.SRC[num % MinGenerator.SRC.length] + ',';
      num = Math.floor(num / MinGenerator.SRC.length);
      c = true;
    }

    const r = num % MinGenerator.SRC.length;
    result += MinGenerator.SRC[c ? r - 1: r];

    return result.split(',').reverse().join('');
  }
}

/**
 * Using regular expressions.
 */
class FromRegexGenerator extends AbstractSchemaGenerator {
  constructor(protected regexp: RegExp) {
    super();
    this.regexp = regexp;
  }

  generate(propNameList: string[], modifier: IFunction<string, string>): string[] {
    const result: string[] = [];
    let count = 1;
    for (const item of propNameList) {
      let str = item.replace(/^t@\w_/, '');
      str = str.replace(this.regexp, ',')
      .split(',')
      .map(x => x.replace(/\W|_/g,''))
      .map(x => x.charAt(0))
      .join('');
      let newName = modifier.apply(str);
      while (result.indexOf(newName) !== -1) {
        newName += count++;
      }
      result.push(newName);
    }
    return result;
  }

}

/**
 * Utility class to join all implemented schema generators.
 */
export class GeneratorUtil {

  public static INCLUDE_TYPE_DESCRIPTORS = true;
  /**
   * Generates the names of the properties with the minimum length.
   */
  // eslint-disable-next-line
  public static readonly MIN_LENGTH: IBiFunction<any, IFunction<string, string>, record> = new MinGenerator();
  /**
   * Transforms the original property name into a shorter version.
   */
  // eslint-disable-next-line
  public static readonly DEFAULT: IBiFunction<any, IFunction<string, string>, record> = new FromRegexGenerator(/(?<=[a-zA-Z0-9])(?=[A-Z_]|\W)/g);
}


export enum Key {
  PROP_NAME = '_propName',
  SCHEMA = '_sch',
  DEFAULT_VALUE = '_defVal',
  KEEP_DEFAULT_VALUE = '_keepDef',
  DESERIALIZE_FUNCTION = '_desFunc',
  SERIALIZE_FUNCTION = '_serFunc',
  CONVERTER = '_conv',
  DEBUGGER = '_debugger' // Only for debugging.
}

export enum TypeDescriptor {
  /* eslint-disable @typescript-eslint/no-explicit-any */
  ARRAY = <any>'t@a_',
  OBJECT = <any>'t@o_',
  STRING = <any>'t@s_',
  NUMBER = <any>'t@n_',
  BOOLEAN = <any>'t@b_',
  DATE = <any>'t@d_',
  NONE = <any>''
  /* eslint-disable @typescript-eslint/no-explicit-any */
}



import {Convert} from "../../../src/func/converters";
import {INativePropSchema, IObjectPropSchema, Schema} from "../../../src/schema";
import {TypeDescriptor} from "../../../src/schema/enums";


describe('Schema.getSchema :::: ', () => {


  const JSON_SCHEMA = {
    't@b_boolean_property': 'bp',
    't@n_number_property': {
      _propName: 'np'
    },
    't@n_number_property_d': {
      _propName: 'npd',
      _defVal: 100
    },
    't@n_number_property_d_k': {
      _propName: 'npdk',
      _defVal: 100,
      _keepDef: true
    },
    't@n_number_property_d_n_k': {
      _propName: 'npdnk',
      _defVal: 100,
      _keepDef: false
    },
    't@b_boolean_property_2': {
      _propName: 'bp2',
      _conv: {
        _serFunc: Convert.FromBoolean.To_0_Or_1,
        _desFunc: Convert.ToBoolean.From_0_Or_1
      }
    },
    't@o_object_property': {
      _propName: 'op',
      _sch: {
        't@n_prop1': 'np',
        't@b_prop2': 'bp',
        't@d_prop3': 'dp',
        't@a_prop4': 'ap',
      }
    }


  };

  const SCHEMA: IObjectPropSchema = Schema.getSchema(JSON_SCHEMA);

  it('Root.', () => {
    expect(SCHEMA.parent).toBeNull();
    expect(SCHEMA.propInSch).toBe('');
    expect(SCHEMA.propNewName).toBe('');
    expect(SCHEMA.propInObj).toBe('$');
    expect(SCHEMA.typeDesc).toBe(TypeDescriptor.OBJECT);
    expect(SCHEMA.map.size).not.toBe(0);
  });

  it('Plain native property.', () => {
    expect(SCHEMA.map.has('boolean_property')).toBeTrue();
    const NATIVE_SCHEMA: INativePropSchema = (SCHEMA.map.get('boolean_property') as INativePropSchema);
    expect(NATIVE_SCHEMA.parent).toEqual(SCHEMA);
    expect(NATIVE_SCHEMA.hasOwnProperty('map')).toBeFalse();
    expect(NATIVE_SCHEMA.propInSch).toBe('t@b_boolean_property');
    expect(NATIVE_SCHEMA.propNewName).toBe('bp');
    expect(NATIVE_SCHEMA.propInObj).toBe('boolean_property');
    expect(NATIVE_SCHEMA.typeDesc).toBe(TypeDescriptor.BOOLEAN);
    expect(NATIVE_SCHEMA.hasDefVal).toBeFalse();
    expect(NATIVE_SCHEMA.keepDef).toBeFalse();
    expect(NATIVE_SCHEMA.defVal).toBeNull();
    expect(NATIVE_SCHEMA.hasConv).toBeFalse();
    expect(NATIVE_SCHEMA.serFn).toBeNull();
    expect(NATIVE_SCHEMA.desFn).toBeNull();
  });

  it('Native property (_defVal).', () => {
    expect(SCHEMA.map.has('number_property_d')).toBeTrue();
    const NATIVE_SCHEMA: INativePropSchema = (SCHEMA.map.get('number_property_d') as INativePropSchema);
    expect(NATIVE_SCHEMA.parent).toEqual(SCHEMA);
    expect(NATIVE_SCHEMA.hasOwnProperty('map')).toBeFalse();
    expect(NATIVE_SCHEMA.propInSch).toBe('t@n_number_property_d');
    expect(NATIVE_SCHEMA.propNewName).toBe('npd');
    expect(NATIVE_SCHEMA.propInObj).toBe('number_property_d');
    expect(NATIVE_SCHEMA.typeDesc).toBe(TypeDescriptor.NUMBER);
    expect(NATIVE_SCHEMA.hasDefVal).toBeTrue();
    expect(NATIVE_SCHEMA.keepDef).toBeFalse();
    expect(NATIVE_SCHEMA.defVal).toBe(100);
    expect(NATIVE_SCHEMA.hasConv).toBeFalse();
    expect(NATIVE_SCHEMA.serFn).toBeNull();
    expect(NATIVE_SCHEMA.desFn).toBeNull();
  });

  it('Native property (_defVal + _keepDef).', () => {
    expect(SCHEMA.map.has('number_property_d_k')).toBeTrue();
    const NATIVE_SCHEMA: INativePropSchema = (SCHEMA.map.get('number_property_d_k') as INativePropSchema);
    expect(NATIVE_SCHEMA.parent).toEqual(SCHEMA);
    expect(NATIVE_SCHEMA.hasOwnProperty('map')).toBeFalse();
    expect(NATIVE_SCHEMA.propInSch).toBe('t@n_number_property_d_k');
    expect(NATIVE_SCHEMA.propNewName).toBe('npdk');
    expect(NATIVE_SCHEMA.propInObj).toBe('number_property_d_k');
    expect(NATIVE_SCHEMA.typeDesc).toBe(TypeDescriptor.NUMBER);
    expect(NATIVE_SCHEMA.hasDefVal).toBeTrue();
    expect(NATIVE_SCHEMA.keepDef).toBeTrue();
    expect(NATIVE_SCHEMA.defVal).toBe(100);
    expect(NATIVE_SCHEMA.hasConv).toBeFalse();
    expect(NATIVE_SCHEMA.serFn).toBeNull();
    expect(NATIVE_SCHEMA.desFn).toBeNull();
  });

  it('Native property (_defVal + not_keepDef).', () => {
    expect(SCHEMA.map.has('number_property_d_n_k')).toBeTrue();
    const NATIVE_SCHEMA: INativePropSchema = (SCHEMA.map.get('number_property_d_n_k') as INativePropSchema);
    expect(NATIVE_SCHEMA.parent).toEqual(SCHEMA);
    expect(NATIVE_SCHEMA.hasOwnProperty('map')).toBeFalse();
    expect(NATIVE_SCHEMA.propInSch).toBe('t@n_number_property_d_n_k');
    expect(NATIVE_SCHEMA.propNewName).toBe('npdnk');
    expect(NATIVE_SCHEMA.propInObj).toBe('number_property_d_n_k');
    expect(NATIVE_SCHEMA.typeDesc).toBe(TypeDescriptor.NUMBER);
    expect(NATIVE_SCHEMA.hasDefVal).toBeTrue();
    expect(NATIVE_SCHEMA.keepDef).toBeFalse();
    expect(NATIVE_SCHEMA.defVal).toBe(100);
    expect(NATIVE_SCHEMA.hasConv).toBeFalse();
    expect(NATIVE_SCHEMA.serFn).toBeNull();
    expect(NATIVE_SCHEMA.desFn).toBeNull();
  });

  it('Native property (_conv).', () => {
    expect(SCHEMA.map.has('boolean_property_2')).toBeTrue();
    const NATIVE_SCHEMA: INativePropSchema = (SCHEMA.map.get('boolean_property_2') as INativePropSchema);
    expect(NATIVE_SCHEMA.parent).toEqual(SCHEMA);
    expect(NATIVE_SCHEMA.hasOwnProperty('map')).toBeFalse();
    expect(NATIVE_SCHEMA.propInSch).toBe('t@b_boolean_property_2');
    expect(NATIVE_SCHEMA.propNewName).toBe('bp2');
    expect(NATIVE_SCHEMA.propInObj).toBe('boolean_property_2');
    expect(NATIVE_SCHEMA.typeDesc).toBe(TypeDescriptor.BOOLEAN);
    expect(NATIVE_SCHEMA.hasDefVal).toBeFalse();
    expect(NATIVE_SCHEMA.keepDef).toBeFalse();
    expect(NATIVE_SCHEMA.defVal).toBeNull();
    expect(NATIVE_SCHEMA.hasConv).toBeTrue();
    expect(NATIVE_SCHEMA.serFn).not.toBeNull();
    expect(NATIVE_SCHEMA.desFn).not.toBeNull();
  });

  it('Object property schema.', () => {
    expect(SCHEMA.map.has('object_property')).toBeTrue();
    const OBJECT_SCHEMA: IObjectPropSchema = (SCHEMA.map.get('object_property') as IObjectPropSchema);
    expect(OBJECT_SCHEMA.parent).toEqual(SCHEMA);
    expect(OBJECT_SCHEMA.hasOwnProperty('hasDefVal')).toBeTrue();
    expect(OBJECT_SCHEMA.hasOwnProperty('keepDef')).toBeTrue();
    expect(OBJECT_SCHEMA.hasOwnProperty('defVal')).toBeTrue();
    expect(OBJECT_SCHEMA.hasOwnProperty('hasConv')).toBeFalse();
    expect(OBJECT_SCHEMA.hasOwnProperty('serFn')).toBeFalse();
    expect(OBJECT_SCHEMA.hasOwnProperty('desFn')).toBeFalse();
    expect(OBJECT_SCHEMA.propInSch).toBe('t@o_object_property');
    expect(OBJECT_SCHEMA.propNewName).toBe('op');
    expect(OBJECT_SCHEMA.propInObj).toBe('object_property');
    expect(OBJECT_SCHEMA.typeDesc).toBe(TypeDescriptor.OBJECT);
    expect(OBJECT_SCHEMA.map.size).toBe(4);
    expect(OBJECT_SCHEMA.map.has('prop1')).toBeTrue();
    expect(OBJECT_SCHEMA.map.has('prop2')).toBeTrue();
    expect(OBJECT_SCHEMA.map.has('prop3')).toBeTrue();
    expect(OBJECT_SCHEMA.map.has('prop4')).toBeTrue();
    // Get one nested
    const NATIVE_SCHEMA: INativePropSchema = (OBJECT_SCHEMA.map.get('prop1') as INativePropSchema);
    expect(NATIVE_SCHEMA.parent).toEqual(OBJECT_SCHEMA);
    expect(NATIVE_SCHEMA.hasOwnProperty('map')).toBeFalse();
    expect(NATIVE_SCHEMA.propInSch).toBe('t@n_prop1');
    expect(NATIVE_SCHEMA.propNewName).toBe('np');
    expect(NATIVE_SCHEMA.propInObj).toBe('prop1');
    expect(NATIVE_SCHEMA.typeDesc).toBe(TypeDescriptor.NUMBER);
    expect(NATIVE_SCHEMA.hasDefVal).toBeFalse();
    expect(NATIVE_SCHEMA.keepDef).toBeFalse();
    expect(NATIVE_SCHEMA.defVal).toBeNull();
    expect(NATIVE_SCHEMA.hasConv).toBeFalse();
    expect(NATIVE_SCHEMA.serFn).toBeNull();
    expect(NATIVE_SCHEMA.desFn).toBeNull();
  });

});

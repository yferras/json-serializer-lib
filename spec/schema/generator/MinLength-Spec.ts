import {GeneratorUtil} from '../../../src/schema/generator';
import {StringModifier} from '../../../src/func/str';
import {ARRAY} from './data';



describe('Schema :::: GeneratorUtil: ', () => {

  it('MIN_LENGTH', () => {

    expect(
      GeneratorUtil.MIN_LENGTH.apply(ARRAY)
    )
    .toEqual(
      {
        't@a_Projects': {
          '_propName': 'a',
          '_sch': {
            't@a_@tags': 'a',
            't@b_finished': 'b',
            't@d_last_deployment_date': 'c',
            't@n_time-40': 'd',
            't@n_time40': 'e',
            't@n_worked-time.months': 'f',
            't@n_worked.time-months': 'g',
            't@n_worked.time@months': 'h',
            't@n_workedTimeMonths': 'i',
            't@s_name': 'j'
          }
        },
        't@o_Developer': {
          '_propName': 'b',
          '_sch': {
            't@n_experience-time-in-years': 'a',
            't@s_dev-status': 'b',
            't@s_firstName': 'c',
            't@s_last_name': 'd'
          }
        }
      }
    );
  });


  it('MIN_LENGTH :::: UPPER_CASE modifier', () => {
    expect(
      GeneratorUtil.MIN_LENGTH.apply(
        ARRAY,
        StringModifier.UPPER_CASE
      )
    )
    .toEqual(
      {
        't@a_Projects': {
          '_propName': 'A',
          '_sch': {
            't@a_@tags': 'A',
            't@b_finished': 'B',
            't@d_last_deployment_date': 'C',
            't@n_time-40': 'D',
            't@n_time40': 'E',
            't@n_worked-time.months': 'F',
            't@n_worked.time-months': 'G',
            't@n_worked.time@months': 'H',
            't@n_workedTimeMonths': 'I',
            't@s_name': 'J'
          }
        },
        't@o_Developer': {
          '_propName': 'B',
          '_sch': {
            't@n_experience-time-in-years': 'A',
            't@s_dev-status': 'B',
            't@s_firstName': 'C',
            't@s_last_name': 'D'
          }
        }
      }
    );
  });

});

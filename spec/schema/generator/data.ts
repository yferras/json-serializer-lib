/**
 * JSON to test the schema generators.
 *
 * 1 - Test the correct conversion from the property original name to its reduced name.
 *        * From camel case
 *        * From snake case
 *        * If the key has strange characters.
 * 2 - Test the collision resolution.
 */
export const ARRAY = [
  {
    Developer: {
      firstName: 'value', // Camel case
      last_name: 'value', // Snake case
      'dev-status': 'value', // Special case '-'
      'experience-time-in-years': 10,
    },
    Projects: [
      {
        name: 'value',
        '@tags': [ 'tag1', 'tag2' ],  // Special case '@'
        workedTimeMonths: 10,         // Camel case.
        'worked.time-months': 10,     // Special case '.' and '-'
        'worked.time@months': 10,     // Special case '.' and '@'
        last_deployment_date: new Date() // Snake case
      },
      {
        name: 'value',
        '@tags': [ 'tag1', 'tag2', 'tag3' ],
        'worked.time@months': 5,
        'worked-time.months': 5,
        finished: true,
        'time-40': 4,
        'time40': 4,
      }
    ]
  },
];


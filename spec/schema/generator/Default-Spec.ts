import {StringModifier} from '../../../src/func/str';
import {GeneratorUtil} from '../../../src/schema/generator';
import {ARRAY} from './data';



describe('Schema :::: GeneratorUtil: ', () => {

  it('DEFAULT', () => {
    expect(
      GeneratorUtil.DEFAULT.apply(ARRAY)
    )
    .toEqual(
      {
        't@o_Developer': {
          '_propName': 'D',
          '_sch': {
            't@s_dev-status': 'ds',
            't@n_experience-time-in-years': 'etiy',
            't@s_firstName': 'fN',
            't@s_last_name': 'ln'
          }
        },
        't@a_Projects': {
          '_propName': 'P',
          '_sch': {
            't@a_@tags': 't',
            't@b_finished': 'f',
            't@d_last_deployment_date': 'ldd',
            't@s_name': 'n',
            't@n_time-40': 't4',
            't@n_time40': 't1',
            't@n_worked-time.months': 'wtm',
            't@n_worked.time-months': 'wtm2',
            't@n_worked.time@months': 'wtm3',
            't@n_workedTimeMonths': 'wTM'
          }
        }
      }
    );
  });

  it('DEFAULT :::: LOWER_CASE modifier', () => {
    expect(
      GeneratorUtil.DEFAULT.apply(
        ARRAY,
        StringModifier.LOWER_CASE
      )
    )
    .toEqual(
      {
        't@o_Developer': {
          '_propName': 'd',
          '_sch': {
            't@s_dev-status': 'ds',
            't@n_experience-time-in-years': 'etiy',
            't@s_firstName': 'fn',
            't@s_last_name': 'ln'
          }
        },
        't@a_Projects': {
          '_propName': 'p',
          '_sch': {
            't@a_@tags': 't',
            't@b_finished': 'f',
            't@d_last_deployment_date': 'ldd',
            't@s_name': 'n',
            't@n_time-40': 't4',
            't@n_time40': 't1',
            't@n_worked-time.months': 'wtm',
            't@n_worked.time-months': 'wtm2',
            't@n_worked.time@months': 'wtm3',
            't@n_workedTimeMonths': 'wtm4'
          }
        }
      }
    );
  });



  it('DEFAULT :::: UPPER_CASE modifier', () => {
    expect(
      GeneratorUtil.DEFAULT.apply(
        ARRAY,
        StringModifier.UPPER_CASE
      )
    )
    .toEqual(
      {
        't@o_Developer': {
          '_propName': 'D',
          '_sch': {
            't@s_dev-status': 'DS',
            't@n_experience-time-in-years': 'ETIY',
            't@s_firstName': 'FN',
            't@s_last_name': 'LN'
          }
        },
        't@a_Projects': {
          '_propName': 'P',
          '_sch': {
            't@a_@tags': 'T',
            't@b_finished': 'F',
            't@d_last_deployment_date': 'LDD',
            't@s_name': 'N',
            't@n_time-40': 'T4',
            't@n_time40': 'T1',
            't@n_worked-time.months': 'WTM',
            't@n_worked.time-months': 'WTM2',
            't@n_worked.time@months': 'WTM3',
            't@n_workedTimeMonths': 'WTM4'
          }
        }
      }
    );
  });

});

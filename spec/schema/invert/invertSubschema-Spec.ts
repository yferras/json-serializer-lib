import { Schema } from '../../../src/schema';



describe('Schema.invert :::: Subschema: ', () => {


  it('', () => {

    expect(
      Schema.invert(
        {
          property: {
            _propName: 'p'
          }
        }
      )
    )
    .toEqual(
      {
        p: {
          _propName: 'property'
        }
      }
    );
  });


  it('User default value', () => {

    expect(
      Schema.invert(
        {
          property: {
            _propName: 'p',
            _defVal: 3.14
          }
        }
      )
    )
    .toEqual(
      {
        p: {
          _propName: 'property',
          _defVal: 3.14
        }
      }
    );
  });

  it('converter', () => {
    let sch: any = {
      property: {
        _propName: 'p',
        _conv: {
          _serFunc: (v: number):string => ''+v,
          _desFunc: (v: string):number => +v
        }
      }
    };

    let invSch: any = Schema.invert(sch);

    expect('p' in invSch).toBeTrue();
    expect(invSch.p._propName).toBe('property');

    // NOTE: the _serFunc and _desFunc are not interchanged, on purpose.
    expect(sch.property._conv._serFunc(5)).toBe('5');
    expect(invSch.p._conv._serFunc(5)).toBe('5');
    expect(sch.property._conv._desFunc('5')).toBe(5);
    expect(invSch.p._conv._desFunc('5')).toBe(5);
  });


});

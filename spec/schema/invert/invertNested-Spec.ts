import { Schema } from '../../../src/schema';



describe('Schema.invert :::: Nested: ', () => {


  it('Level 1.', () => {

    expect(Schema.invert({
      // Level 0
      property_1: 'p1',
      property_2: {
        // Level 1
        _propName: 'p2',
        _sch: {
          // Subschema Level 1
          nestedPropertyL_1_1: 'nPL11',
          nestedpropertyL_1_2: 'nPL12',
          nestedpropertyL_1_3: 'nPL13',
          nestedpropertyL_1_4: 'nPL14'
        }
      }
    }))
    .toEqual({
      // Level 0
      p1: 'property_1',
      p2: {
        // Level 1
        _propName: 'property_2',
        _sch: {
          // Subschema Level 1
          nPL11: 'nestedPropertyL_1_1',
          nPL12: 'nestedpropertyL_1_2',
          nPL13: 'nestedpropertyL_1_3',
          nPL14: 'nestedpropertyL_1_4'
        }
      }
    });

  });

  it('Level 2.', () => {

    expect(Schema.invert({
      // Level 0
      property_1: 'p1',
      property_2: {
        // Level 1
        _propName: 'p2',
        _sch: {
          // Subschema Level 1
          nestedPropertyL_1_1: 'nPL11',
          nestedPropertyL_1_2: {
            // Level 2
            _propName: 'nPL12',
            _sch: {
              // Subschema Level 2
              nestedpropertyL_2_1: 'nPL21',
              nestedpropertyL_2_2: 'nPL22',
              nestedpropertyL_2_3: 'nPL23'
            }
          }
        }
      }
    }))
    .toEqual({
      // Level 0
      p1: 'property_1',
      p2: {
        // Level 1
        _propName: 'property_2',
        _sch: {
          // Subschema Level 1
          nPL11: 'nestedPropertyL_1_1',
          nPL12: {
            // Level 2
            _propName: 'nestedPropertyL_1_2',
            _sch: {
              // Subschema Level 2
              nPL21: 'nestedpropertyL_2_1',
              nPL22: 'nestedpropertyL_2_2',
              nPL23: 'nestedpropertyL_2_3'
            }
          }
        }
      }
    });

  });


});

import {FunctionUtil} from "../../../src/func";
import {Schema} from "../../../src/schema";

describe('Schema.merge :::: Subschema: ', () => {


  it('Mix schemas.', () => {

    let sch1: any = {
      prop_1: 'p1',
      prop_2: {
        _propName: 'p2',
        _sch: {
          prop_1_1: 'p11',
          prop_1_2: {
            _propName: 'p12',
            _sch: {
              a: 'a',
              b: 'b',
              z: {
                _propName: 'z',
                _defVal: '<NONE>',
                _keepDef: true,
              }
            }
          },
          prop_1_3: 'p13',
          prop_1_4: 'p14',
        }
      },
      prop_X: 'pX',
      prop_Z: 'pZ'
    };

    let sch2: any = {
      prop_1: 'p1',
      prop_2: {
        _propName: 'p2',
        _sch: {
          prop_1_1: 'p11',
          prop_1_2: {
            _propName: 'p12',
            _sch: {
              b: 'b',
              c: 'c',
              z: {
                _propName: 'z',
                _conv: {
                  _serFunc: FunctionUtil.IDENTITY.apply,
                  _desFunc: FunctionUtil.IDENTITY.apply
                }
              }
            }
          },
          prop_1_a: 'p1a',
          prop_1_b: 'p1b',
          prop_1_c: 'p1c',
        }
      },
      prop_K: 'pK'
    };

    Schema.merge(sch1, sch2);

    expect(sch2).toEqual({
      prop_1: 'p1',
      prop_2: {
        _propName: 'p2',
        _sch: {
          prop_1_1: 'p11',
          prop_1_2: {
            _propName: 'p12',
            _sch: {
              a: 'a',
              b: 'b',
              c: 'c',
              z: {
                _propName: 'z',
                _defVal: '<NONE>',
                _keepDef: true,
                _conv: {
                  _serFunc: FunctionUtil.IDENTITY.apply,
                  _desFunc: FunctionUtil.IDENTITY.apply
                }
              }
            }
          },
          prop_1_3: 'p13',
          prop_1_4: 'p14',
          prop_1_a: 'p1a',
          prop_1_b: 'p1b',
          prop_1_c: 'p1c',
        }
      },
      prop_K: 'pK',
      prop_X: 'pX',
      prop_Z: 'pZ',
    });
  });




});

import {Schema} from "../../../src/schema";

describe('Schema.merge :::: Simple: ', () => {

  it('Empty.', () => {
    let empty1: any = {};
    let empty2: any = {};

    Schema.merge(empty1, empty2);
    expect(empty2).toEqual({});
  });

  it('Not empty into empty.', () => {
    let sch: any = {
      prop_1: 'p_1',
      prop_2: 'p_2',
    };
    let empty: any = {};

    Schema.merge(sch, empty);
    expect(empty).toEqual({
      prop_1: 'p_1',
      prop_2: 'p_2'
    });
  });


  it('Empty into not empty.', () => {
    let sch: any = {
      prop_1: 'p_1',
      prop_2: 'p_2',
    };
    let empty: any = {};

    Schema.merge(empty, sch);
    expect(sch).toEqual({
      prop_1: 'p_1',
      prop_2: 'p_2'
    });
  });

  it('Common properties.', () => {
    let sch1: any = {
      prop_1: 'p_1',
      prop_2: 'p_2',
    };
    let sch2: any = {
      prop_1: 'p_1',
      prop_2: 'p_2',
    };

    Schema.merge(sch2, sch1);
    expect(sch1).toEqual({
      prop_1: 'p_1',
      prop_2: 'p_2'
    });
  });

  it('No common properties.', () => {
    let sch1: any = {
      prop_1: 'p_1',
    };
    let sch2: any = {
      prop_2: 'p_2',
    };

    Schema.merge(sch2, sch1);
    expect(sch1).toEqual({
      prop_1: 'p_1',
      prop_2: 'p_2'
    });
  });

  it('Mix properties.', () => {
    let sch1: any = {
      prop_1: 'p_1',
      prop_2: 'p_2',
      prop_3: 'p_3',
    };
    let sch2: any = {
      prop_2: 'p_2',
      prop_4: 'p_4',
    };

    Schema.merge(sch2, sch1);
    expect(sch1).toEqual({
      prop_1: 'p_1',
      prop_2: 'p_2',
      prop_3: 'p_3',
      prop_4: 'p_4',
    });
  });





});

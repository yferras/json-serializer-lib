import {TypeDescriptor as TD} from "../../../src/schema/enums";
import {Schema} from "../../../src/schema";


describe('Schema.getDefaultValueByType :::: Get: ', () => {


  it('array.', () => {
    expect(Schema.getDefaultValueByType(TD.ARRAY)).toEqual([]);
  });

  it('date.', () => {
    expect(Schema.getDefaultValueByType(TD.DATE)).toBe(null);
  });

  it('number.', () => {
    expect(Schema.getDefaultValueByType(TD.NUMBER)).toBe(0);
  });

  it('object.', () => {
    expect(Schema.getDefaultValueByType(TD.OBJECT)).toEqual({});
  });

  it('string.', () => {
    expect(Schema.getDefaultValueByType(TD.STRING)).toBe('');
  });

  it('boolean.', () => {
    expect(Schema.getDefaultValueByType(TD.BOOLEAN)).toBe(false);
  });

  it('NONE.', () => {
    expect(Schema.getDefaultValueByType(TD.NONE)).toBe(null);
  });
});

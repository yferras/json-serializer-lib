import {TypeDescriptor as TD} from "../../../src/schema/enums";
import {Schema} from "../../../src/schema";


describe('Schema.getTypeDescriptor :::: Get: ', () => {


  it('array.', () => {
    expect(Schema.getTypeDescriptor('t@a_')).toBe(TD.ARRAY);
    expect(Schema.getTypeDescriptor('t@a_property')).toBe(TD.ARRAY);
  });

  it('date.', () => {
    expect(Schema.getTypeDescriptor('t@d_')).toBe(TD.DATE);
    expect(Schema.getTypeDescriptor('t@d_property')).toBe(TD.DATE);
  });

  it('number.', () => {
    expect(Schema.getTypeDescriptor('t@n_')).toBe(TD.NUMBER);
    expect(Schema.getTypeDescriptor('t@n_property')).toBe(TD.NUMBER);
  });

  it('object.', () => {
    expect(Schema.getTypeDescriptor('t@o_')).toBe(TD.OBJECT);
    expect(Schema.getTypeDescriptor('t@o_property')).toBe(TD.OBJECT);
  });

  it('string.', () => {
    expect(Schema.getTypeDescriptor('t@s_')).toBe(TD.STRING);
    expect(Schema.getTypeDescriptor('t@s_property')).toBe(TD.STRING);
  });

  it('boolean.', () => {
    expect(Schema.getTypeDescriptor('t@b_')).toBe(TD.BOOLEAN);
    expect(Schema.getTypeDescriptor('t@b_property')).toBe(TD.BOOLEAN);
  });

  it('NONE.', () => {
    expect(Schema.getTypeDescriptor('t@x_')).toBe(TD.NONE);
    expect(Schema.getTypeDescriptor('t@x_property')).toBe(TD.NONE);
    expect(Schema.getTypeDescriptor('property')).toBe(TD.NONE);
  });
});

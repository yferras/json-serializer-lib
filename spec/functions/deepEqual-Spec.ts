import { FunctionUtil } from '../../src/func';

const OBJECT_A = {
  a: 1,
  array1: [1, 2, 3],
  array2: [],
  array3: ['a', 1, false, {}, undefined, null],
  nested_object1: {
    p1: false,
    nested_object2: {
      p1: false,
      array: [],
      array1: [{ a: 1 }, { b: 2 }, { c: 'abc' }, { d: [1, 2, 3] }],
    }
  }
};

const OBJECT_B = {
  a: 1,
  array1: [1, 2, 3],
  array2: [],
  array3: ['a', 1, false, {}, undefined, null],
  nested_object1: {
    p1: false,
    nested_object2: {
      p1: false,
      array: [],
      array1: [{ a: 1 }, { b: 2 }, { c: 'abc' }, { d: [1, 1, 3] }], // different
    }
  }
};

describe('Functions :::: DeepEqual: ', () => {

  //////////////////////////////////////////////////////////////////////////////
  it('Different types', () => {
    expect(FunctionUtil.DEEP_EQUALS.apply(1, true)).toBeFalse();
    expect(FunctionUtil.DEEP_EQUALS.apply(true, 1)).toBeFalse();
    expect(FunctionUtil.DEEP_EQUALS.apply('abc', 1)).toBeFalse();
    expect(FunctionUtil.DEEP_EQUALS.apply(1, 'abc')).toBeFalse();
    expect(FunctionUtil.DEEP_EQUALS.apply({}, 1)).toBeFalse();
    expect(FunctionUtil.DEEP_EQUALS.apply(1, {})).toBeFalse();
    expect(FunctionUtil.DEEP_EQUALS.apply([], 1)).toBeFalse();
    expect(FunctionUtil.DEEP_EQUALS.apply(1, [])).toBeFalse();
    expect(FunctionUtil.DEEP_EQUALS.apply([], {})).toBeFalse();
    expect(FunctionUtil.DEEP_EQUALS.apply({}, [])).toBeFalse();
    expect(FunctionUtil.DEEP_EQUALS.apply(null, undefined)).toBeFalse();
  });

  it('Same types but different values', () => {
    expect(FunctionUtil.DEEP_EQUALS.apply(1, 2)).toBeFalse();
    expect(FunctionUtil.DEEP_EQUALS.apply(true, false)).toBeFalse();
    expect(FunctionUtil.DEEP_EQUALS.apply('abc', '123')).toBeFalse();
    expect(FunctionUtil.DEEP_EQUALS.apply([1, 2, 3], [3, 2, 1])).toBeFalse();
    expect(FunctionUtil.DEEP_EQUALS.apply({ a: 1 }, { a: 2 })).toBeFalse();
  });

  it('Same types same values', () => {
    expect(FunctionUtil.DEEP_EQUALS.apply(null, null)).toBeTrue();
    expect(FunctionUtil.DEEP_EQUALS.apply(undefined, undefined)).toBeTrue();
    expect(FunctionUtil.DEEP_EQUALS.apply(1, 1)).toBeTrue();
    expect(FunctionUtil.DEEP_EQUALS.apply(true, true)).toBeTrue();
    expect(FunctionUtil.DEEP_EQUALS.apply('abc', 'abc')).toBeTrue();
    expect(FunctionUtil.DEEP_EQUALS.apply([1, 2, 3], [1, 2, 3])).toBeTrue();
    expect(FunctionUtil.DEEP_EQUALS.apply({ a: 1 }, { a: 1 })).toBeTrue();
  });

  it('Same object (more complex)', () => {
    expect(FunctionUtil.DEEP_EQUALS.apply([OBJECT_A, OBJECT_B], [OBJECT_A, OBJECT_B])).toBeTrue();
    expect(FunctionUtil.DEEP_EQUALS.apply(OBJECT_A, OBJECT_A)).toBeTrue();
  });

  it('Almost same object (more complex)', () => {
    expect(FunctionUtil.DEEP_EQUALS.apply([OBJECT_A], [OBJECT_B])).toBeFalse();
    expect(FunctionUtil.DEEP_EQUALS.apply(OBJECT_A, OBJECT_B)).toBeFalse();
  });

});

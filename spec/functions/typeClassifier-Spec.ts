import { InputType, FunctionUtil } from '../../src/func';

describe('Functions :::: TypeClassifier: ', () => {
  it('Array.', () => {
    expect(FunctionUtil.TYPE_CLASSIFIER.apply([])).toBe(InputType.ARRAY);
  });

  it('Object.', () => {
    expect(FunctionUtil.TYPE_CLASSIFIER.apply({})).toBe(InputType.OBJECT);
  });

  it('String.', () => {
    expect(FunctionUtil.TYPE_CLASSIFIER.apply('text')).toBe(InputType.NATIVE);
  });

  it('Number(Integer).', () => {
    expect(FunctionUtil.TYPE_CLASSIFIER.apply(0)).toBe(InputType.NATIVE);
  });

  it('Number(Double).', () => {
    expect(FunctionUtil.TYPE_CLASSIFIER.apply(Math.PI)).toBe(InputType.NATIVE);
  });

  it('Classify input: Boolean.', () => {
    expect(FunctionUtil.TYPE_CLASSIFIER.apply(true)).toBe(InputType.NATIVE);
  });

  it('Classify input: Undefined.', () => {
    expect(FunctionUtil.TYPE_CLASSIFIER.apply(undefined)).toBe(InputType.NATIVE);
  });

  it('Classify input: Null.', () => {
    expect(FunctionUtil.TYPE_CLASSIFIER.apply(null)).toBe(InputType.NATIVE);
  });

  it('Classify input: Date.', () => {
    expect(FunctionUtil.TYPE_CLASSIFIER.apply(new Date())).toBe(InputType.NATIVE);
  });
})

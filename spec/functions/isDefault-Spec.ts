import {ConfigurationUtil} from '../../src/config';
import { FunctionUtil } from '../../src/func';

describe('Commons :::: IS_DEFAULT: ', () => {

  it('Returns true', () => {
    expect(FunctionUtil.IS_DEFAULT.apply([]))
      .toBeTrue();
    expect(FunctionUtil.IS_DEFAULT.apply({}))
      .toBeTrue();
    expect(FunctionUtil.IS_DEFAULT.apply(''))
      .toBeTrue();
    expect(FunctionUtil.IS_DEFAULT.apply(false))
      .toBeTrue();
    expect(FunctionUtil.IS_DEFAULT.apply(null))
      .toBeTrue();
    expect(FunctionUtil.IS_DEFAULT.apply(undefined))
      .toBeTrue();
    expect(FunctionUtil.IS_DEFAULT.apply(0))
      .toBeTrue();
  });

  it('Returns false', () => {
    expect(FunctionUtil.IS_DEFAULT.apply([1, 2, 3]))
      .toBeFalse();
    expect(FunctionUtil.IS_DEFAULT.apply({ a: 1 }))
      .toBeFalse();
    expect(FunctionUtil.IS_DEFAULT.apply('abcd'))
      .toBeFalse();
    expect(FunctionUtil.IS_DEFAULT.apply(true))
      .toBeFalse();
    expect(FunctionUtil.IS_DEFAULT.apply(1))
      .toBeFalse();
  });

  it('Alternatives returns true', () => {
    expect(FunctionUtil.IS_DEFAULT.apply(1, [1]))
      .toBeTrue();
    expect(FunctionUtil.IS_DEFAULT.apply(true, [true]))
      .toBeTrue();
    expect(FunctionUtil.IS_DEFAULT.apply(
      { default1: 1, default2: 2 },
      [
        { default1: 1, default2: 2 }
      ]))
      .toBeTrue();
  });


});

describe('Commons :::: IS_DEFAULT (custom defaults): ', () => {

  beforeAll(() => {
    ConfigurationUtil.DEFAULTS.NUMBER = -1;
    ConfigurationUtil.DEFAULTS.STRING = '<NONE>';
    ConfigurationUtil.DEFAULTS.BOOLEAN= true;
  });

  afterAll(() => {
    ConfigurationUtil.DEFAULTS.NUMBER = 0;
    ConfigurationUtil.DEFAULTS.STRING = '';
    ConfigurationUtil.DEFAULTS.BOOLEAN= false;
  });



  it('Returns true', () => {
    expect(FunctionUtil.IS_DEFAULT.apply([]))
      .toBeTrue();
    expect(FunctionUtil.IS_DEFAULT.apply({}))
      .toBeTrue();
    expect(FunctionUtil.IS_DEFAULT.apply('<NONE>'))
      .toBeTrue();
    expect(FunctionUtil.IS_DEFAULT.apply(true))
      .toBeTrue();
    expect(FunctionUtil.IS_DEFAULT.apply(null))
      .toBeTrue();
    expect(FunctionUtil.IS_DEFAULT.apply(undefined))
      .toBeTrue();
    expect(FunctionUtil.IS_DEFAULT.apply(-1))
      .toBeTrue();
  });

  it('Returns false', () => {
    expect(FunctionUtil.IS_DEFAULT.apply([1, 2, 3]))
      .toBeFalse();
    expect(FunctionUtil.IS_DEFAULT.apply({ a: 1 }))
      .toBeFalse();
    expect(FunctionUtil.IS_DEFAULT.apply(''))
      .toBeFalse();
    expect(FunctionUtil.IS_DEFAULT.apply(false))
      .toBeFalse();
    expect(FunctionUtil.IS_DEFAULT.apply(0))
      .toBeFalse();
  });



});

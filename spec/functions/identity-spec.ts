import { FunctionUtil } from '../../src/func';


describe('Functions - Identity:', () => {
  it("Identity.", () => {
    expect(FunctionUtil.IDENTITY.apply(0)).toBe(0);
    expect(FunctionUtil.IDENTITY.apply(1000)).toBe(1000);
    expect(FunctionUtil.IDENTITY.apply('abc')).toBe('abc');
    expect(
      FunctionUtil.IDENTITY.apply(
        [
          'abc',
          1,
          true,
          undefined,
          null]
      )
    ).toEqual(
    [
      'abc',
      1,
      true,
      undefined,
      null
    ]
    );
  });


})


export const SCHEMA = {
  property_01: 'p01',
  property_02: 'p02',
  property_03: 'p03',
  property_04: 'p04',
  property_05: {
    _propName: 'p05',
    _sch: {
      subProperty01: 'sp01'
    }
  }
};

export const OBJECT = {
  property_01: 1,                     // Property defined in the schema.
  property_02: 'OK...',               // Property defined in the schema.
  property_03: true,                  // Property defined in the schema.
  property_04: [ 2, 4, 6 ],           // Property defined in the schema.
  property_05: {                      // Property defined in the schema.
    subProperty01: 'OK... OK... :)'
  },
  unknown_prop_01: 100,
  unknown_prop_02: 'Hello WORLD!!!',
  unknown_prop_03: true,
  unknown_prop_04: [ 1, 3, 5 ],
  unknown_prop_05: {
    subProperty01: 'Happy coding!!!'
  }
};

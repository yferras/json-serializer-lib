import {
  ConfigurationUtil as ConfUtil,
  StrategyForUnknownProperty as S4UnkProp
} from '../../../src/config';
import {Model} from '../../../src/serializer';
import {OBJECT, SCHEMA} from './data';


describe('Model.serialize :::: Ignore Unknown Properties.', () => {


  it('Throws an error.', () => {
    expect(() => {
      Model.serialize(
        OBJECT,
        SCHEMA,
        ConfUtil.settingsBuilderFor.SERIALIZER
        .strategyForUnknownProperty(S4UnkProp.THROW_ERROR)
        .build()
      ).serializedJSON
    })
    .toThrowError();
  });


});

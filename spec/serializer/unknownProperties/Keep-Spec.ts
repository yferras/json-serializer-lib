import { Model } from '../../../src/serializer';
import {
  ConfigurationUtil as ConfUtil,
  StrategyForUnknownProperty as S4UnkProp
} from '../../../src/config';
import { OBJECT, SCHEMA } from './data';


describe('Model.serialize :::: Ignore Unknown Properties.', () => {

  it('Explicit configuration.', () => {
    expect(
      Model.serialize(
        OBJECT,
        SCHEMA,
        ConfUtil.settingsBuilderFor.SERIALIZER
        .strategyForUnknownProperty(S4UnkProp.KEEP)
        .build()
      ).serializedJSON
    )
    .toEqual(
      {
        p01: 1,                     // Property defined in the schema.
        p02: 'OK...',               // Property defined in the schema.
        p03: true,                  // Property defined in the schema.
        p04: [ 2, 4, 6 ],           // Property defined in the schema.
        p05: {                      // Property defined in the schema.
          sp01: 'OK... OK... :)'
        },
        unknown_prop_01: 100,
        unknown_prop_02: 'Hello WORLD!!!',
        unknown_prop_03: true,
        unknown_prop_04: [ 1, 3, 5 ],
        unknown_prop_05: {
          subProperty01: 'Happy coding!!!'
        }

      });
  });

});

import { Model } from '../../../src/serializer';
import {
  ConfigurationUtil as ConfUtil,
  StrategyForUnknownProperty as S4UnkProp
} from '../../../src/config';
import { OBJECT, SCHEMA } from './data';


describe('Model.serialize :::: Ignore Unknown Properties.', () => {


  it('Default configuration.', () => {
    expect(
      Model.serialize(
        OBJECT,
        SCHEMA
      ).serializedJSON
    )
    .toEqual(
      {
        p01: 1,                     // Property defined in the schema.
        p02: 'OK...',               // Property defined in the schema.
        p03: true,                  // Property defined in the schema.
        p04: [ 2, 4, 6 ],           // Property defined in the schema.
        p05: {                      // Property defined in the schema.
          sp01: 'OK... OK... :)'
        },
      });
  });

  it('Explicit configuration.', () => {
    expect(
      Model.serialize(
        OBJECT,
        SCHEMA,
        ConfUtil.settingsBuilderFor.SERIALIZER
        .strategyForUnknownProperty(S4UnkProp.IGNORE)
        .build()
      ).serializedJSON
    )
    .toEqual(
      {
        p01: 1,                     // Property defined in the schema.
        p02: 'OK...',               // Property defined in the schema.
        p03: true,                  // Property defined in the schema.
        p04: [ 2, 4, 6 ],           // Property defined in the schema.
        p05: {                      // Property defined in the schema.
          sp01: 'OK... OK... :)'
        },
      });
  });
});

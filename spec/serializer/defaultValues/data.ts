export const DATE_VALUE = new Date('2022-03-16T00:00:00.000Z');

export const OBJECT =  {
  ///////////////////////////////////////////////////////
  // Predefined default values
  property_predef_val_01: 0,           // Zero.
  property_predef_val_02: '',          // Empty string.
  property_predef_val_03: false,       // False.
  property_predef_val_04: [],          // Empty array.
  property_predef_val_05: {},          // Empty object.
  property_predef_val_06: undefined,   // UNDEFINED.
  property_predef_val_07: null,        // NULL.
  property_predef_val_08: {
    subProperty01: 0                    // Nested Zero.
  },
  property_predef_val_09: {
    subProperty01: ''                   // Nested empty string.
  },
  property_predef_val_10: {
    subProperty01: false                // Nested false.
  },
  property_predef_val_11: {
    subProperty01: []                   // Nested empty array.
  },
  property_predef_val_12: {
    subProperty01: {}                   // Nested empty object.
  },
  property_predef_val_13: {
    subProperty01: undefined            // Nested UNDEFINED.
  },
  property_predef_val_14: {
    subProperty01: null                 // Nested NULL.
  },
  ///////////////////////////////////////////////////////
  // User defined default values
  property_user_def_val_01: -1,        // A number different from 0.
  property_user_def_val_02: '<NONE>',  // A not empty string.
  property_user_def_val_03: true,      // A not false value.
  property_user_def_val_04: [ -100 ],  // A not empty array as default value.
  property_user_def_val_05: {          // A not empty object as default value.
    this_prop_dont_need_to_be_mapped: '<N/A>'
  },
  ///////////////////////////////////////////////////////
  // Values
  property01: Math.PI,
  property02: 'Hello WORLD!!!',
  property03: true,
  property04: [ 1, 2, 3 ],
  property05: {
    subProperty01: 'Happy coding',
    subProperty02: DATE_VALUE
  }
};

export const SCHEMA = {
  ///////////////////////////////////////////////////////
  // Predefined default values
  't@n_property_predef_val_01': 'ppv01',
  't@s_property_predef_val_02': 'ppv02',
  't@b_property_predef_val_03': 'ppv03',
  't@a_property_predef_val_04': 'ppv04',
  't@o_property_predef_val_05': 'ppv05',
  't@n_property_predef_val_06': {_propName: 'ppv06', _defVal: undefined},
  't@o_property_predef_val_07': {_propName: 'ppv07', _defVal: null},
  't@o_property_predef_val_08': {
    _propName: 'ppv08',
    _sch: {
      't@n_subProperty01': 'sp01'
    }
  },
  't@o_property_predef_val_09': {
    _propName: 'ppv09',
    _sch: {
      't@s_subProperty01': 'sp01'
    }
  },
  't@o_property_predef_val_10': {
    _propName: 'ppv10',
    _sch: {
      't@b_subProperty01': 'sp01'
    }
  },
  't@o_property_predef_val_11': {
    _propName: 'ppv11',
    _sch: {
      't@a_subProperty01': 'sp01'
    }
  },
  't@o_property_predef_val_12': {
    _propName: 'ppv12',
    _sch: {
      't@o_subProperty01': 'sp01'
    }
  },
  't@o_property_predef_val_13': {
    _propName: 'ppv13',
    _sch: {
      't@n_subProperty01': 'sp01'
    }
  },
  't@o_property_predef_val_14': {
    _propName: 'ppv14',
    _sch: {
      't@o_subProperty01': 'sp01'
    }
  },
  ///////////////////////////////////////////////////////
  // User defined default values
  't@n_property_user_def_val_01':{
    _propName: 'puv01',
    _defVal: -1
  },
  't@s_property_user_def_val_02':{
    _propName: 'puv02',
    _defVal: '<NONE>'
  },
  't@b_property_user_def_val_03':{
    _propName: 'puv03',
    _defVal: true
  },
  't@a_property_user_def_val_04':{
    _propName: 'puv04',
    _defVal: [ -100  ]
  },
  't@o_property_user_def_val_05':{
    _propName: 'puv05',
    _defVal: { // The default value is an entire object.
      this_prop_dont_need_to_be_mapped: '<N/A>'
    }
  },
  ///////////////////////////////////////////////////////
  // Values
  't@n_property01': 'p01',
  't@s_property02': 'p02',
  't@b_property03': 'p03',
  't@a_property04': 'p04',
  't@o_property05': {
    _propName: 'p05',
    _sch: {
      't@s_subProperty01': 'sp01',
      't@d_subProperty02': 'sp02'
    }
  }
};



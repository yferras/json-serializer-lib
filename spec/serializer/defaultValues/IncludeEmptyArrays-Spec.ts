import { Model } from '../../../src/serializer';
import { ConfigurationUtil as ConfUtil } from '../../../src/config';
import { DATE_VALUE, OBJECT,SCHEMA } from './data';


describe('Model.serialize :::: Include: ', () => {

  it('Empty arrays.', () => {
    expect(
      Model.serialize(
        OBJECT,
        SCHEMA,
        ConfUtil.settingsBuilderFor.SERIALIZER // Default config.
        .includeEmptyArrays(true)   // keep all empty arrays.
        .build()
      ).serializedJSON
    )
    .toEqual(
      {
        ppv04: [], // <-- [] will be included.
        ppv11: {
          sp01: [] // <-- [] will be included.
        },
        p01: Math.PI,
        p02: 'Hello WORLD!!!',
        p03: true,
        p04: [ 1, 2, 3 ],
        p05: {
          sp01: 'Happy coding',
          sp02: DATE_VALUE
        }
      }
    );
  });

});

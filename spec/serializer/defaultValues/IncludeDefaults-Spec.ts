import {ConfigurationUtil} from '../../../src/config';
import { Model } from '../../../src/serializer';
import { DATE_VALUE, OBJECT, SCHEMA } from './data';

describe('Model.serialize :::: Include:', () => {

  it('Default values.', () => {
    expect(
      Model.serialize(
        OBJECT,
        SCHEMA,
        ConfigurationUtil.settingsBuilderFor.SERIALIZER.includeDefaultValues(true).build()
      ).serializedJSON
    )
    .toEqual(
      {
        ppv01: 0,
        ppv02: '',
        ppv03: false,
        ppv04: [],
        ppv05: {},
        ppv06: undefined,
        ppv07: null,
        ppv08: {
          sp01: 0
        },
        ppv09: {
          sp01: ''
        },
        ppv10: {
          sp01: false
        },
        ppv11: {
          sp01: []
        },
        ppv12: {
          sp01: {}
        },
        ppv13: {
          sp01: undefined
        },
        ppv14: {
          sp01: null
        },
        puv01: -1,
        puv02: '<NONE>',
        puv03: true,
        puv04: [
          -100
        ],
        puv05: {
          this_prop_dont_need_to_be_mapped: '<N/A>'
        },
        p01: 3.141592653589793,
        p02: 'Hello WORLD!!!',
        p03: true,
        p04: [
          1,
          2,
          3
        ],
        p05: {
          sp01: 'Happy coding',
          sp02: DATE_VALUE
        }
      }
    );
  });

});

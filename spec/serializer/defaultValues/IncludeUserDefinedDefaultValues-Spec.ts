import { Model } from '../../../src/serializer';
import { ConfigurationUtil as ConfUtil } from '../../../src/config';
import { DATE_VALUE, OBJECT, SCHEMA } from './data';


describe('Model.serialize :::: Include: ', () => {

  it('User defined default values.', () => {
    expect(
      Model.serialize(
        OBJECT,
        SCHEMA,
        ConfUtil.settingsBuilderFor.SERIALIZER // Default config.
        .includeUserDefinedDefaultValues(true)   // keep all user defined default values.
        .build()
      ).serializedJSON
    )
    .toEqual(
      {
        puv01: -1,        // user defined default value will be included.
        puv02: '<NONE>',  // user defined default value will be included.
        puv03: true,      // user defined default value will be included.
        puv04: [ -100 ],  // user defined default value will be included.
        puv05: {          // user defined default value will be included.
          this_prop_dont_need_to_be_mapped: '<N/A>'
        },
        p01: Math.PI,
        p02: 'Hello WORLD!!!',
        p03: true,
        p04: [ 1, 2, 3 ],
        p05: {
          sp01: 'Happy coding',
          sp02: DATE_VALUE
        }
      }
    );
  });

});

import { Model } from '../../../src/serializer';
import { ConfigurationUtil as ConfUtil } from '../../../src/config';
import { DATE_VALUE, OBJECT, SCHEMA } from './data';


describe('Model.serialize :::: Include: ', () => {

  it('FALSE values.', () => {
    expect(
      Model.serialize(
        OBJECT,
        SCHEMA,
        ConfUtil.settingsBuilderFor.SERIALIZER // Default config.
        .includeDefaultBoolean(true)   // keep all false values.
        .build()
      ).serializedJSON
    )
    .toEqual(
      {
        ppv03: false, // <-- false will be included.
        ppv10: {
          sp01: false // <-- false will be included.
        },
        p01: Math.PI,
        p02: 'Hello WORLD!!!',
        p03: true,
        p04: [ 1, 2, 3 ],
        p05: {
          sp01: 'Happy coding',
          sp02: DATE_VALUE
        }
      }
    );
  });

});

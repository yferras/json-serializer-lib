import { Model } from '../../../src/serializer';
import { ConfigurationUtil as ConfUtil } from '../../../src/config';
import { DATE_VALUE, OBJECT, SCHEMA } from './data';


describe('Model.serialize :::: Include: ', () => {

  it('NULL values.', () => {
    expect(
      Model.serialize(
        OBJECT,
        SCHEMA,
        ConfUtil.settingsBuilderFor.SERIALIZER // Default config.
        .includeNullValues(true)   // keep all null values.
        .build()
      ).serializedJSON
    )
    .toEqual(
      {
        ppv07: null, // <-- null will be included.
        ppv14: {
          sp01: null // <-- null  will be included.
        },
        p01: Math.PI,
        p02: 'Hello WORLD!!!',
        p03: true,
        p04: [ 1, 2, 3 ],
        p05: {
          sp01: 'Happy coding',
          sp02: DATE_VALUE
        }
      }
    );
  });

});

import { Model } from '../../../src/serializer';
import { ConfigurationUtil as ConfUtil } from '../../../src/config';
import { DATE_VALUE, OBJECT, SCHEMA } from './data';


describe('Model.serialize :::: Include: ', () => {

  it('Empty strings.', () => {
    expect(
      Model.serialize(
        OBJECT,
        SCHEMA,
        ConfUtil.settingsBuilderFor.SERIALIZER // Default config.
        .includeDefaultString(true)   // keep all empty strings.
        .build()
      ).serializedJSON
    )
    .toEqual(
      {
        ppv02: '', // <-- '' will be included.
        ppv09: {
          sp01: '' // <-- '' will be included.
        },
        p01: Math.PI,
        p02: 'Hello WORLD!!!',
        p03: true,
        p04: [ 1, 2, 3 ],
        p05: {
          sp01: 'Happy coding',
          sp02: DATE_VALUE
        }
      }
    );
  });

});

import { Model } from '../../../src/serializer';
import { ConfigurationUtil as ConfUtil } from '../../../src/config';
import { DATE_VALUE, OBJECT,SCHEMA } from './data';


describe('Model.serialize :::: Include: ', () => {

  it('Empty objects.', () => {
    expect(
      Model.serialize(
        OBJECT,
        SCHEMA,
        ConfUtil.settingsBuilderFor.SERIALIZER // Default config.
        .includeEmptyObjects(true)   // keep all empty objects.
        .build()
      ).serializedJSON
    )
    .toEqual(
      {
        ppv05: {}, // <-- {} will be included.
        ppv08: {},
        ppv09: {},
        ppv10: {},
        ppv11: {},
        ppv12: {
          sp01: {} // <-- {} will be included.
        },
        ppv13: {},
        ppv14: {},
        p01: Math.PI,
        p02: 'Hello WORLD!!!',
        p03: true,
        p04: [ 1, 2, 3 ],
        p05: {
          sp01: 'Happy coding',
          sp02: DATE_VALUE
        }
      }
    );
  });

});

import { Model } from '../../../src/serializer';
import { ConfigurationUtil as ConfUtil } from '../../../src/config';
import { DATE_VALUE, OBJECT, SCHEMA } from './data';


describe('Model.serialize :::: Include: ', () => {

  it('UNDEFINED values.', () => {
    expect(
      Model.serialize(
        OBJECT,
        SCHEMA,
        ConfUtil.settingsBuilderFor.SERIALIZER // Default config.
        .includeUndefinedValues(true)   // keep all undefined values.
        .build()
      ).serializedJSON
    )
    .toEqual(
      {
        ppv06: undefined, // <-- undefined will be included.
        ppv13: {
          sp01: undefined // <-- undefined will be included.
        },
        p01: Math.PI,
        p02: 'Hello WORLD!!!',
        p03: true,
        p04: [ 1, 2, 3 ],
        p05: {
          sp01: 'Happy coding',
          sp02: DATE_VALUE
        }
      }
    );
  });

});

import { Convert } from '../../../src/func/converters';
import { Model } from '../../../src/serializer';
import { ConfigurationUtil as ConfUtil } from '../../../src/config';


describe('Boolean converter.', () => {

  const OBJECT = {
    property_01: true,
    property_02: false,
    property_03: {
      nested_prop_01: true,
      nested_prop_02: false,
      nested_prop_03: {
        nested_nested_prop_01: true,
        nested_nested_prop_02: false,
      }
    }
  };

  const SCHEMA = {
    't@b_property_01': 'p1',
    't@b_property_02': 'p2',

    't@o_property_03': {
      _propName: 'p3',
      _sch: {
        't@b_nested_prop_01': 'np1',
        't@b_nested_prop_02': 'np2',
        't@o_nested_prop_03': {
          _propName: 'np3',
          _sch: {
            't@b_nested_nested_prop_01': 'nnp1',
            't@b_nested_nested_prop_02': 'nnp2'
          }
        },
      }
    },
  }

  it('Using custom configuration "setBooleanConverter" to "Convert.FromBoolean.To_0_Or_1".', () => {
    expect(Model.serialize(
      OBJECT,
      SCHEMA,
      ConfUtil.settingsBuilderFor.SERIALIZER
      .includeDefaultBoolean(true)
      .setBooleanConverter(Convert.FromBoolean.To_0_Or_1) // Globalized boolean converter.
      .build()
    ).serializedJSON)
    .toEqual({
      p1: 1,
      p2: 0,
      p3: {
        np1: 1,
        np2: 0,
        np3: {
          nnp1: 1,
          nnp2: 0
        }
      }
    })
  });

});

import { Convert } from '../../../src/func/converters';
import { Model } from '../../../src/serializer';
import { ConfigurationUtil as ConfUtil } from '../../../src/config';


describe('Boolean converter.', () => {
  const boolToInt = Convert.FromBoolean.To_0_Or_1.func;

  const intToBool = Convert.ToBoolean.From_0_Or_1.func;

  const OBJECT = {
    property_01: true,
    property_02: false,
    property_03: {
      nested_prop_01: true,
      nested_prop_02: false,
      nested_prop_03: {
        nested_nested_prop_01: true,
        nested_nested_prop_02: false,
      }
    }
  };

  const SCHEMA = {
    't@b_property_01': {
      _propName: 'p1',
      _conv: {
        _serFunc: boolToInt,
        _desFunc: intToBool
      }
    },
    't@b_property_02': {
      _propName: 'p2',
      _conv: {
        _serFunc: boolToInt,
        _desFunc: intToBool
      }
    },

    't@o_property_03': {
      _propName: 'p3',
      _sch: {
        't@b_nested_prop_01': {
          _propName: 'np1',
          _conv: {
            _serFunc: boolToInt,
            _desFunc: intToBool
          }
        },
        't@b_nested_prop_02': {
          _propName: 'np2',
          _conv: {
            _serFunc: boolToInt,
            _desFunc: intToBool
          }
        },
        't@o_nested_prop_03': {
          _propName: 'np3',
          _sch: {
            't@b_nested_nested_prop_01': {
              _propName: 'nnp1',
              _conv: {
                _serFunc: boolToInt,
                _desFunc: intToBool
              }
            },
            't@b_nested_nested_prop_02': {
              _propName: 'nnp2',
              _conv: {
                _serFunc: boolToInt,
                _desFunc: intToBool
              }
            }
          }
        },
      }
    },
  }

  it('Customized converter', () => {
    expect(Model.serialize(
      OBJECT,
      SCHEMA,
      ConfUtil.settingsBuilderFor.SERIALIZER
      .includeDefaultBoolean(true)
      .build()
    ).serializedJSON)
    .toEqual({
      p1: 1,
      p2: 0,
      p3: {
        np1: 1,
        np2: 0,
        np3: {
          nnp1: 1,
          nnp2: 0
        }
      }
    })
  });

});

import {ConfigurationUtil} from '../../src/config';
import {checkDefinedMethods} from './util';

const builder = ConfigurationUtil.settingsBuilderFor.SERIALIZER;

describe('Serialization settings builder.', () => {

  checkDefinedMethods(builder);

});

import {ConfigurationUtil, StrategyForUnknownProperty} from '../../src/config';
import {FunctionUtil} from '../../src/func';
import {checkDefaultConfig} from './util';



const config = ConfigurationUtil.globalDesConfig;

describe('Deserialization Default Config.', () => {

  checkDefaultConfig(config, true, StrategyForUnknownProperty.KEEP);

  it('"toBooleanFrom" should be: IDENTITY function.',() => {
    expect(config.toBooleanFrom).toBe(FunctionUtil.IDENTITY);
  });

  it('"toDateFrom" should be: IDENTITY function.',() => {
    expect(config.toDateFrom).toBe(FunctionUtil.IDENTITY);
  });

  it('"useNullInsteadOfDefaulValues" should be: false.',() => {
    expect(config.useNullInsteadOfDefaulValues).toBe(false);
  });


});

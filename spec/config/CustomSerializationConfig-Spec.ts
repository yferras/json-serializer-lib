import { ConfigurationUtil, StrategyForUnknownProperty, ISerializationConfig } from '../../src/config';
import { Convert } from '../../src/func/converters';

const config: ISerializationConfig =
  ConfigurationUtil.settingsBuilderFor.SERIALIZER
.includeDefaultValues(true)
.includeStats(true)
.includeNullValues(true)
.includeDefaultBoolean(true)
.strategyForUnknownProperty(StrategyForUnknownProperty.THROW_ERROR)
.setBooleanConverter(Convert.FromBoolean.To_0_Or_1)
.setDateConverter(Convert.FromDate.ToTimeInMillis)
.build();

describe('Custom Serialization Config.', () => {

  it('"includeDefaultValues" should be: true.', () => {
    expect(config.includeDefaultValues).toBeTrue();
  });

  it('"includeStats" should be: true.', () => {
    expect(config.includeStats).toBeTrue();
  });

  it('"includeEmptyArrays" should be: true.', () => {
    expect(config.includeEmptyArrays).toBeTrue();
  });

  it('"includeEmptyObjects" should be: true.', () => {
    expect(config.includeEmptyObjects).toBeTrue();
  });

  it('"includeNullValues" should be: true.', () => {
    expect(config.includeNullValues).toBeTrue();
  });

  it('"includeUndefinedValues" should be: true.', () => {
    expect(config.includeUndefinedValues).toBeTrue();
  });

  it('"includeDefaultBoolean" should be: true.', () => {
    expect(config.includeDefaultBoolean).toBeTrue();
  });

  it('"includeDefaultString" should be: true.', () => {
    expect(config.includeDefaultString).toBeTrue();
  });

  it('"includeDefaultNumber" should be: true.', () => {
    expect(config.includeDefaultNumber).toBeTrue();
  });

  it('"includeUserDefinedDefaultValues" should be: true.', () => {
    expect(config.includeUserDefinedDefaultValues).toBeTrue();
  });

  it('"toDateFrom" should be: Convert.FromDate.ToTimeInMillis.', () => {
    expect(config.fromDateTo).toBe(Convert.FromDate.ToTimeInMillis);
  });

  it('"toBooleanFrom" should be: Convert.FromBoolean.To_0_Or_1.', () => {
    expect(config.fromBooleanTo).toBe(Convert.FromBoolean.To_0_Or_1);
  });

  it('"strategyForUnknownProperty" should be: THROW_ERROR.', () => {
    expect(config.strategyForUnknownProperty).toBe(StrategyForUnknownProperty.THROW_ERROR);
  });

});

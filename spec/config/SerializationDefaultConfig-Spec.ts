import { ConfigurationUtil, StrategyForUnknownProperty } from '../../src/config';
import {FunctionUtil} from '../../src/func';
import {checkDefaultConfig} from './util';

const config = ConfigurationUtil.globalSerConfig;

describe('Serialization Default Config.', () => {

  checkDefaultConfig(config, false, StrategyForUnknownProperty.IGNORE);

  it('"fromBooleanTo" should be: IDENTITY function.',() => {
    expect(config.fromBooleanTo).toBe(FunctionUtil.IDENTITY);
  });

  it('"fromDateTo" should be: IDENTITY function.',() => {
    expect(config.fromDateTo).toBe(FunctionUtil.IDENTITY);
  });

});


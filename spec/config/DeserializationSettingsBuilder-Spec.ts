import { ConfigurationUtil } from '../../src/config';
import {checkDefinedMethods} from './util';

const builder = ConfigurationUtil.settingsBuilderFor.DESERIALIZER;

describe('Deserialization settings builder.', () => {

  checkDefinedMethods(builder);

});

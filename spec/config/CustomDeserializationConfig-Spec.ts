import { ConfigurationUtil, StrategyForUnknownProperty, IDeserializationConfig } from '../../src/config';
import { Convert } from '../../src/func/converters';

const config: IDeserializationConfig =
  ConfigurationUtil.settingsBuilderFor.DESERIALIZER
.includeDefaultValues(false)
.includeStats(true)
.includeDefaultNumber(true)
.setBooleanConverter(Convert.ToBoolean.From_0_Or_1)
.setDateConverter(Convert.ToDate.FromTimeInMillis)
.build();

describe('Custom Deserialization Config.', () => {

  it('"includeDefaultValues" should be: false.', () => {
    expect(config.includeDefaultValues).toBeFalse();
  });

  it('"includeStats" should be: true.', () => {
    expect(config.includeStats).toBeTrue();
  });

  it('"includeEmptyArrays" should be: false.', () => {
    expect(config.includeEmptyArrays).toBeFalse();
  });

  it('"includeEmptyObjects" should be: false.', () => {
    expect(config.includeEmptyObjects).toBeFalse();
  });

  it('"includeNullValues" should be: false.', () => {
    expect(config.includeNullValues).toBeFalse();
  });

  it('"includeUndefinedValues" should be: false.', () => {
    expect(config.includeUndefinedValues).toBeFalse();
  });

  it('"includeDefaultBoolean" should be: false.', () => {
    expect(config.includeDefaultBoolean).toBeFalse();
  });

  it('"includeDefaultString" should be: false.', () => {
    expect(config.includeDefaultString).toBeFalse();
  });

  it('"includeDefaultNumber" should be: true.', () => {
    expect(config.includeDefaultNumber).toBeTrue();
  });

  it('"includeUserDefinedDefaultValues" should be: false.', () => {
    expect(config.includeUserDefinedDefaultValues).toBeFalse();
  });

  it('"includeDefaultBoolean" should be: false.', () => {
    expect(config.includeDefaultBoolean).toBeFalse();
  });

  it('"toDateFrom" should be: Convert.ToDate.FromTimeInMillis.', () => {
    expect(config.toDateFrom).toBe(Convert.ToDate.FromTimeInMillis);
  });

  it('"toBooleanFrom" should be: Convert.ToBoolean.From_0_Or_1.', () => {
    expect(config.toBooleanFrom).toBe(Convert.ToBoolean.From_0_Or_1);
  });

  it('"strategyForUnknownProperty" should be: KEEP.', () => {
    expect(config.strategyForUnknownProperty).toBe(StrategyForUnknownProperty.KEEP);
  });

});

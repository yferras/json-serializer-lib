import {ConfigurationUtil} from "../../../src/config";
import {Model} from "../../../src/serializer";

const person = {
    firstName: 'Jhon',
    lastName: 'Doe',
    age: 35,
    weight: -1,
    height: -1
};

const personSchema = {
    't@s_firstName': 'fn',
    't@s_lastName': 'ln',
    't@n_age': 'a',
    't@n_weight': {
      _propName: 'w',
      _defVal: -1
    },
    't@n_height': {
      _propName: 'h',
      _defVal: -1
    }
};



const serializedJSON_1 = Model.serialize(person, personSchema).serializedJSON;

const serializedJSON_2 = Model.serialize(
  person, personSchema,
  ConfigurationUtil.settingsBuilderFor.SERIALIZER
  .includeDefaultValues(true)
  .build()
).serializedJSON


const deserializedJSON_1 = Model.deserialize(serializedJSON_1, personSchema).deserializedJSON;

const deserializedJSON_2 = Model.deserialize(
  serializedJSON_2, personSchema,
  ConfigurationUtil.settingsBuilderFor.DESERIALIZER
  .includeDefaultValues(false)
  .build()
).deserializedJSON;

describe('Demo 7', () => {

  it('Defualt serilize.', () => {
    expect(serializedJSON_1).toEqual(
      {
        fn: 'Jhon',
        ln: 'Doe',
        a: 35
      }
    );
  });

  it('Extra config serilize.', () => {
    expect(serializedJSON_2).toEqual(
      {
        fn: 'Jhon',
        ln: 'Doe',
        a: 35,
        w: -1,
        h: -1
      }
    );
  });


  it('Defualt deserilize.', () => {
    expect(deserializedJSON_1).toEqual(
      {
        firstName: 'Jhon',
        lastName: 'Doe',
        age: 35,
        weight: -1,
        height: -1
      }
    );
  });

  it('Extra config deserilize.', () => {
    expect(deserializedJSON_2).toEqual(
      {
        firstName: 'Jhon',
        lastName: 'Doe',
        age: 35
      }
    );
  });
});


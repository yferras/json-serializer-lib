import {ConfigurationUtil} from '../../../src/config';
import {Model} from '../../../src/serializer'


// the true value is converted to one.
// the false value is converted to zero.
const boolToInt = (val: boolean): number => {
  return val == true ? 1 : 0;
};

// the 1 value is converted to true.
// the 0 value is converted to false.
// Otherwise an error will be thrown.
const intToBool = (val: number): boolean => {
  if (val == 0) {
    return false;
  }
  if (val == 1) {
    return true;
  }
  throw new Error(`${val} is not a valid value for conversion`);
};

const SCHEMA = {
  't@b_positive': {
    _propName: 'p',
    _conv: {
      _serFunc: boolToInt,
      _desFunc: intToBool
    }
  },
  't@b_negative': {
    _propName: 'n',
    _conv: {
      _serFunc: boolToInt,
      _desFunc: intToBool
    }
  }
};

const serializedJSON = Model.serialize(
  {
    positive: true,
    negative: false
  },
  SCHEMA,
  ConfigurationUtil.settingsBuilderFor.SERIALIZER
  .includeDefaultBoolean(true)
  .build()
).serializedJSON;

const deserializedJSON = Model.deserialize(serializedJSON, SCHEMA).deserializedJSON;

describe('Demo 2', () => {


  it('Serialize.', () => {
    expect(serializedJSON).toEqual({
      p: 1,
      n: 0
    });
  });

  it('Deserialize.', () => {
    expect(deserializedJSON).toEqual({
      positive: true,
      negative: false
    });
  });
});


import {ConfigurationUtil} from '../../../src/config';
import {Model} from '../../../src/serializer'


const dayOfWeek = [
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday',
  'Sunday'
];


const dowToInt = (val: string): number => {
  const r = dayOfWeek.indexOf(val);
  if (r !== -1) {
    return r;
  }
  throw new Error(`${val} is not valid`);
};

const intToDoW = (val: number): string => {
  return dayOfWeek[val];
};


export const SCHEMA = {
  't@s_day_of_the_week': {
    _propName: 'dow',
    _conv: {
      _serFunc: dowToInt,
      _desFunc: intToDoW
    }
  }
};

const serializedJSON = Model.serialize(
  {
    day_of_the_week: 'Wednesday',
  },
  SCHEMA
).serializedJSON;

const deserializedJSON = Model.deserialize(serializedJSON, SCHEMA).deserializedJSON;


describe('Demo 4', () => {


  it('Serialize.', () => {
    expect(serializedJSON).toEqual({
      dow: 2
    });
  });

  it('Deserialize.', () => {
    expect(deserializedJSON).toEqual({
      day_of_the_week: 'Wednesday',
    });
  });
});

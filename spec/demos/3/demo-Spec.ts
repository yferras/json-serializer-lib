import {ConfigurationUtil} from '../../../src/config';
import {Model} from '../../../src/serializer'


const dayOfWeek = [
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday',
  'Sunday'
];

const shortenDoW = (val: string): string => {
  return val.substring(0, 2);
};

const fullDoW = (val: string): string => {
  for (const day of dayOfWeek) {
    if (day.substring(0, 2) === val) {
      return day;
    }
  }
  throw new Error(`${val} is not valid`);
};

export const SCHEMA = {
  't@s_day_of_the_week': {
    _propName: 'dow',
    _conv: {
      _serFunc: shortenDoW,
      _desFunc: fullDoW
    }
  }
};

const serializedJSON = Model.serialize(
  {
    day_of_the_week: 'Wednesday',
  },
  SCHEMA
).serializedJSON;

const deserializedJSON = Model.deserialize(serializedJSON, SCHEMA).deserializedJSON;


describe('Demo 3', () => {


  it('Serialize.', () => {
    expect(serializedJSON).toEqual({
      dow: 'We'
    });
  });

  it('Deserialize.', () => {
    expect(deserializedJSON).toEqual({
    day_of_the_week: 'Wednesday',
  });
  });
});


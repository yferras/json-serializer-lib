import {Model} from '../../../src/serializer'


////////////////////////////////////////////////////
const OBJ_JOB_SCH = {
  't@n_worked_years': {
    _propName: 'wy',
    // Define the custom value to -1
    _defVal: -1
  },
  't@s_title': {
    _propName: 't',
    // Define the custom value to 'UNKNOWN'
    _defVal: 'UNKNOWN'
  }
};

const OBJ_PERSON_SCH = {

  't@o_job': {
    _propName: 'j',
    _sch: OBJ_JOB_SCH
  },

};

/////////////////////////////////////////////////////
// Person
const OBJ_1 = {
  job: {
    // Not matches with the default value
    worked_years: 10,
    // Not matches with the default value
    title: 'Engineer'
  }
};

const OBJ_2 = {
  job: {
    // Default value in the schema
    worked_years: -1,
    // Not matches with the default value
    title: 'Engineer'
  }
};


const OBJ_3 = {
  job: {
    // Default value in the schema
    worked_years: -1,
    // Default value in the schema
    title: 'UNKNOWN'
  }
};


const OBJ_4 = {
  job: {
    // Not matches with the default value
    worked_years: 10,
    // Default value in the schema
    title: 'UNKNOWN'
  }
};


const apply = (obj: any, sch: any): [any, any] => {
  const serializedJSON = Model.serialize(obj, sch).serializedJSON;
  const deserializedJSON = Model.deserialize(serializedJSON, sch).deserializedJSON;
  return [ serializedJSON, deserializedJSON ];
};


describe('Demo 5', () => {

  it('1', () => {

    const [s, d] = apply(OBJ_1, OBJ_PERSON_SCH);

    expect(s).toEqual({
      "j": {
        "wy": 10,
        "t": "Engineer"
      }
    });

    expect(d).toEqual(OBJ_1);
  });

  it('2', () => {

    const [s, d] = apply(OBJ_2, OBJ_PERSON_SCH);

    expect(s).toEqual({
      "j": {
        "t": "Engineer"
      }
    });

    expect(d).toEqual(OBJ_2);
  });

  it('3', () => {

    const [s, d] = apply(OBJ_3, OBJ_PERSON_SCH);

    expect(s).toEqual({});

    expect(d).toEqual(OBJ_3);
  });

  it('4', () => {

    const [s, d] = apply(OBJ_4, OBJ_PERSON_SCH);

    expect(s).toEqual({
      "j": {
        "wy": 10
      }
    });

    expect(d).toEqual(OBJ_4);
  });


});

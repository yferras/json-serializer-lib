import {GeneratorUtil} from '../../../src/schema/generator';
import {Model} from '../../../src/serializer'

import { INPUT } from "./input";
import { SCHEMA } from "./schema";
import { OUTPUT } from "./output";


describe('Demo 1', () => {

  it('Generate schema.', () => {
    expect(GeneratorUtil.DEFAULT.apply(INPUT)).toEqual(SCHEMA);
  });

  it('Serialize.', () => {
    expect(Model.serialize(INPUT, SCHEMA).serializedJSON).toEqual(OUTPUT);
  });

  it('Deserialize.', () => {
    expect(Model.deserialize(OUTPUT, SCHEMA).deserializedJSON).toEqual(INPUT);
  });
});

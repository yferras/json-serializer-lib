/**
 * JSON before send it.
 */

export const INPUT = [
  {
    'first_name': 'Person_1’s name',
    'last_name': 'Person_1’s last name',
    'heigh_cm': 183,
    'weigth_kg': 85,
    'is_a_male': true,
    'birth_date': '1991-06-21T00:00:00.000Z',
    'job': { // Nested object
      'title': 'Job’s title',
      'worked_years': 12
    },
    'amount_of_purchases': 2,
    'purchases': [ // Array of objects
      {
        'product_id': 1093,
        'quantity': 1,
        'product_description': 'Product’s description'
      },
      {
        'product_id': 3,
        'quantity': 5,
        'product_description': 'Product’s description'
      }
    ],
    'addresses': [ // Array of objects
      {
        'zip_code': 765765,
        'street': 'Street’s Name',
        'city': 'City’s Name',
        'state': 'State’s Name',
        'country': 'Country’s Name',
        'is_home': true
      },
      {
        'zip_code': 123546,
        'street': 'Street’s Name',
        'city': 'City’s Name',
        'state': 'State’s Name',
        'country': 'Country’s Name',
        'is_home': false
      }
    ]
  },
  {
    'first_name': 'Person_N’s name',
    'last_name': 'Person_N’s last_name',
    'heigh_cm': 167,
    'weigth_kg': 60,
    'is_a_male': false,
    'birth_date': '1983-02-14T00:00:00.000Z',
    'job': {
      'title': '<NONE>',
      'worked_years': 0
    },
    'amount_of_purchases': 0,
    'purchases': [],
    'addresses': [
      {
        'zip_code': 987654,
        'street': 'Street’s Name',
        'city': 'City’s Name',
        'state': 'State‘s Name',
        'country': 'Country’s Name',
        'is_home': false
      },
      {
        'zip_code': 543278,
        'street': 'Street’s Name',
        'city': 'City’s Name',
        'state': 'State’s Name',
        'country': 'Country’s Name',
        'is_home': true
      }
    ]
  }
];

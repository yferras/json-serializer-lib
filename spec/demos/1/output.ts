/**
 * JSON  after send it.
 */

export const OUTPUT = [
  {
    'fn': 'Person_1’s name',
    'ln': 'Person_1’s last name',
    'hc': 183,
    'wk': 85,
    'iam': true,
    'bd': '1991-06-21T00:00:00.000Z',
    'j': {
      't': 'Job’s title',
      'wy': 12
    },
    'aop': 2,
    'p': [
      {
        'pi': 1093,
        'q': 1,
        'pd': 'Product’s description'
      },
      {
        'pi': 3,
        'q': 5,
        'pd': 'Product’s description'
      }
    ],
    'a': [
      {
        'zc': 765765,
        's2': 'Street’s Name',
        'c': 'City’s Name',
        's': 'State’s Name',
        'c1': 'Country’s Name',
        'ih': true
      },
      {
        'zc': 123546,
        's2': 'Street’s Name',
        'c': 'City’s Name',
        's': 'State’s Name',
        'c1': 'Country’s Name'
      }
    ]
  },
  {
    'fn': 'Person_N’s name',
    'ln': 'Person_N’s last_name',
    'hc': 167,
    'wk': 60,
    'bd': '1983-02-14T00:00:00.000Z',
    'j': {
      't': '<NONE>'
    },
    'a': [
      {
        'zc': 987654,
        's2': 'Street’s Name',
        'c': 'City’s Name',
        's': 'State‘s Name',
        'c1': 'Country’s Name'
      },
      {
        'zc': 543278,
        's2': 'Street’s Name',
        'c': 'City’s Name',
        's': 'State’s Name',
        'c1': 'Country’s Name',
        'ih': true
      }
    ]
  }
];

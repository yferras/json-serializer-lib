import { Convert } from '../../src/func/converters';



describe('Functions - Convert date:', () => {

  const date = new Date();
  const millis = date.getTime();

  it('To milliseconds.', () => {
    expect(Convert.FromDate.ToTimeInMillis.apply(date)).toEqual(millis);
  });


});


import { Convert } from '../../src/func/converters';

describe('Functions - Convert To Boolean From: ', () => {


  it('0 Or 1 when 1 should be true.', () => {
    expect(Convert.ToBoolean.From_0_Or_1.apply(1)).toBeTrue();
  });
  it('0 Or 1 when 0 should be false.', () => {
    expect(Convert.ToBoolean.From_0_Or_1.apply(0)).toBeFalse();
  });


  it('"F" Or "T" when "F" should be false.', () => {
    expect(Convert.ToBoolean.From_F_Or_T.apply('F')).toBeFalse();
  });
  it('"F" Or "T" when "T" should be true.', () => {
    expect(Convert.ToBoolean.From_F_Or_T.apply('T')).toBeTrue();
  });


  it('"N" Or "Y" when "N" should be false.', () => {
    expect(Convert.ToBoolean.From_N_Or_Y.apply('N')).toBeFalse();
  });
  it('"N" Or "Y" when "Y" should be true.', () => {
    expect(Convert.ToBoolean.From_N_Or_Y.apply('Y')).toBeTrue();
  });


  it('From Custom validation error.', () => {
    expect(() => Convert.ToBoolean.FromCustom({})).toThrowError();
  });

  it('From Custom.', () => {
    const functObj  = Convert.ToBoolean.FromCustom({
      A: true,
      B: false,
      10: true,
      100: false,
    });
    expect(functObj.apply('A')).toBeTrue();
    expect(functObj.apply(10)).toBeTrue();
    expect(functObj.apply('B')).toBeFalse();
    expect(functObj.apply(100)).toBeFalse();
    expect(() => functObj.apply('z')).toThrowError();
    expect(() => functObj.apply(1000)).toThrowError();
  });

});

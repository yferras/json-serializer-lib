
import { Convert } from '../../src/func/converters';

describe('Functions - Convert date:', () => {

  const date = new Date();
  const millis = date.getTime();

  it('From milliseconds.', () => {
    expect(Convert.ToDate.FromTimeInMillis.apply(millis)).toEqual(date);
  });


});

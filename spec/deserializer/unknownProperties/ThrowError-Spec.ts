import { Model } from '../../../src/serializer';
import {
  ConfigurationUtil as ConfUtil,
  StrategyForUnknownProperty as S4UnkProp
} from '../../../src/config';
import { SER_OBJECT, SCHEMA } from './data';


describe('Model.deserialize :::: Unknown Properties.', () => {


  it('Throws an error.', () => {
    expect(() => {
      Model.deserialize(
        SER_OBJECT,
        SCHEMA,
        ConfUtil.settingsBuilderFor.DESERIALIZER
        .strategyForUnknownProperty(S4UnkProp.THROW_ERROR)
        .build()
      ).deserializedJSON
    })
    .toThrowError();
  });


});

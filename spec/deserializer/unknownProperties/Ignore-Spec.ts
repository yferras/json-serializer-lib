import { Model } from '../../../src/serializer';
import {
  ConfigurationUtil as ConfUtil,
  StrategyForUnknownProperty as S4UnkProp
} from '../../../src/config';
import { SER_OBJECT, SCHEMA } from './data';


describe('Model.deserialize :::: Unknown Properties.', () => {


  it('Explicit configuration.', () => {
    expect(
      Model.deserialize(
        SER_OBJECT,
        SCHEMA,
        ConfUtil.settingsBuilderFor.DESERIALIZER
        .strategyForUnknownProperty(S4UnkProp.IGNORE)
        .build()
      ).deserializedJSON
    )
    .toEqual(
      {
        property_01: 1,                     // Property defined in the schema.
        property_02: 'OK...',               // Property defined in the schema.
        property_03: true,                  // Property defined in the schema.
        property_04: [ 2, 4, 6 ],           // Property defined in the schema.
        property_05: {                      // Property defined in the schema.
          subProperty01: 'OK... OK... :)'
        },
      });
  });
});

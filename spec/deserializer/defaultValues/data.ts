export const DATE_VALUE = new Date();

export const SER_OBJ = {
  p01: Math.PI,
  p02: 'Hello WORLD!!!',
  p03: true,
  p04: [ 1, 2, 3 ],
  p05: {
    sp01: 'Happy coding',
    sp02: DATE_VALUE,
    sp03: undefined
  }
};

export const SCHEMA = {
  ///////////////////////////////////////////////////////
  // Predefined default values
  't@n_property_predef_val_01': 'ppv01',
  't@s_property_predef_val_02': 'ppv02',
  't@b_property_predef_val_03': 'ppv03',
  't@a_property_predef_val_04': 'ppv04',
  't@o_property_predef_val_05': 'ppv05',
  't@n_property_predef_val_06': 'ppv06',
  't@o_property_predef_val_07': 'ppv07',
  't@o_property_predef_val_08': {
    _propName: 'ppv08',
    _sch: {
      't@n_subProperty01': 'sp01'
    }
  },
  't@o_property_predef_val_09': {
    _propName: 'ppv09',
    _sch: {
      't@s_subProperty01': 'sp01'
    }
  },
  't@o_property_predef_val_10': {
    _propName: 'ppv10',
    _sch: {
      't@b_subProperty01': 'sp01'
    }
  },
  't@o_property_predef_val_11': {
    _propName: 'ppv11',
    _sch: {
      't@a_subProperty01': 'sp01'
    }
  },
  't@o_property_predef_val_12': {
    _propName: 'ppv12',
    _sch: {
      't@o_subProperty01': 'sp01'
    }
  },
  't@o_property_predef_val_13': {
    _propName: 'ppv13',
    _sch: {
      't@n_subProperty01': 'sp01'
    }
  },
  't@o_property_predef_val_14': {
    _propName: 'ppv14',
    _sch: {
      't@o_subProperty01': 'sp01'
    }
  },
  ///////////////////////////////////////////////////////
  // User defined default values
  't@n_property_user_def_val_01':{
    _propName: 'puv01',
    _defVal: -1
  },
  't@s_property_user_def_val_02':{
    _propName: 'puv02',
    _defVal: '<NONE>'
  },
  't@b_property_user_def_val_03':{
    _propName: 'puv03',
    _defVal: true
  },
  't@a_property_user_def_val_04':{
    _propName: 'puv04',
    _defVal: [ -100  ]
  },
  't@o_property_user_def_val_05':{
    _propName: 'puv05',
    _defVal: { // The default value is an entire object.
      this_prop_dont_need_to_be_mapped: '<N/A>'
    }
  },
  ///////////////////////////////////////////////////////
  // Values
  't@n_property01': 'p01',
  't@s_property02': 'p02',
  't@b_property03': 'p03',
  't@a_property04': 'p04',
  't@o_property05': {
    _propName: 'p05',
    _sch: {
      't@s_subProperty01': 'sp01',
      't@d_subProperty02': 'sp02',
      't@n_subProperty03': 'sp03'
    }
  }
};


import { Model } from '../../../src/serializer';
import {DATE_VALUE, SCHEMA, SER_OBJ} from './data';

describe('Model.deserialize :::: Default config.', () => {

  it('', () => {
    expect(
      Model.deserialize(
        SER_OBJ,
        SCHEMA,
      ).deserializedJSON
    )
    .toEqual(
      {
        ///////////////////////////////////////////////////////
        // Predefined default values
        property_predef_val_01: 0,
        property_predef_val_02: '',
        property_predef_val_03: false,
        property_predef_val_04: [],
        property_predef_val_05: {},
        property_predef_val_06: 0,
        property_predef_val_07: {},
        property_predef_val_08: {
          subProperty01: 0
        },
        property_predef_val_09: {
          subProperty01: ''
        },
        property_predef_val_10: {
          subProperty01: false
        },
        property_predef_val_11: {
          subProperty01: []
        },
        property_predef_val_12: {
          subProperty01: {}
        },
        property_predef_val_13: {
          subProperty01: 0
        },
        property_predef_val_14: {
          subProperty01: {}
        },
        ///////////////////////////////////////////////////////
        // User defined default values
        property_user_def_val_01: -1,        // A number different from 0.
        property_user_def_val_02: '<NONE>',  // A not empty string.
        property_user_def_val_03: true,      // A not false value.
        property_user_def_val_04: [ -100 ], // Custom default array
        property_user_def_val_05: { // Custom default object.
          this_prop_dont_need_to_be_mapped: '<N/A>'
        },
        ///////////////////////////////////////////////////////
        // Values
        property01: Math.PI,
        property02: 'Hello WORLD!!!',
        property03: true,
        property04: [ 1, 2, 3 ],
        property05: {
          subProperty01: 'Happy coding',
          subProperty02: DATE_VALUE,
          subProperty03: undefined,
        }
      }
    );
  });

});


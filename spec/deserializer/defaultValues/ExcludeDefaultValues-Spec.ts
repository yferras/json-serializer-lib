import {ConfigurationUtil} from '../../../src/config';
import { Model } from '../../../src/serializer';
import {DATE_VALUE, SCHEMA, SER_OBJ} from './data';

describe('Model.deserialize :::: Exclude:', () => {

  it('Default values.', () => {
    expect(
      Model.deserialize(
        SER_OBJ,
        SCHEMA,
        ConfigurationUtil.settingsBuilderFor.DESERIALIZER.includeDefaultValues(false).build()
      ).deserializedJSON
    )
    .toEqual(
      {
        property01: Math.PI,
        property02: 'Hello WORLD!!!',
        property03: true,
        property04: [ 1, 2, 3 ],
        property05: {
          subProperty01: 'Happy coding',
          subProperty02: DATE_VALUE
        }
      }
    );
  });

});


// karma.conf.js
//
// karma.conf.js
require('ts-node').register({
  compilerOptions: {
    module: 'commonjs'
  }
});
module.exports = function(config) {
  config.set({
    browsers: ["Firefox"],
    files: [
      { pattern: "spec/**/*.ts" },
      { pattern: "src/**/*.ts" },
    ],
    preprocessors: {
      "src/**/*.ts": ["karma-typescript"],
      "spec/**/*.ts": ["karma-typescript"],
    },
    frameworks: [
      "jasmine", "karma-typescript"
    ],

    reporters: ['kjhtml', "progress", "karma-typescript"],

    karmaTypescriptConfig: {
      compilerOptions: {
        allowJs: true,
      },
    },
  });
};

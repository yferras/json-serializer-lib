"use strict";
////////////////////////////////////////////////////////////////////////////////
// Interfaces
////////////////////////////////////////////////////////////////////////////////
Object.defineProperty(exports, "__esModule", { value: true });
exports.FunctionUtil = void 0;
const config_1 = require("../config");
const enums_1 = require("./enums");
////////////////////////////////////////////////////////////////////////////////
// Functions
////////////////////////////////////////////////////////////////////////////////
class IdentityFunction {
    constructor() {
        /* uninstantiable */
    }
    apply(arg) {
        return arg;
    }
}
// This instance of function will be used in multiple context.
// eslint-disable-next-line
IdentityFunction.INSTANCE = new IdentityFunction();
////////////////////////////////////////////////////////////////////////////////
// Type Classify
////////////////////////////////////////////////////////////////////////////////
class TypeClassifier {
    constructor() {
        /* uninstantiable */
    }
    apply(arg) {
        if (arg === null) {
            return enums_1.InputType.NATIVE;
        }
        // Only arrays.
        if (Array.isArray(arg)) {
            return enums_1.InputType.ARRAY;
        }
        if (arg instanceof Date) {
            return enums_1.InputType.NATIVE;
        }
        // Only 'object'.
        if (typeof (arg) === 'object') {
            return enums_1.InputType.OBJECT;
        }
        // Only for: 'string' | 'number' | 'boolean' | 'undefined'.
        if (/(?:string)|(?:number)|(?:boolean)|(?:undefined)/ig.test(typeof (arg))) {
            return enums_1.InputType.NATIVE;
        }
        // If  'symbol' | 'function'.
        return enums_1.InputType.UNKNOWN;
    }
}
TypeClassifier.INSTANCE = new TypeClassifier();
////////////////////////////////////////////////////////////////////////////////
// Deep Equal
////////////////////////////////////////////////////////////////////////////////
class DeepEqualsFunction {
    constructor() {
        /* uninstantiable */
    }
    apply(a, b) {
        const typeOfA = TypeClassifier.INSTANCE.apply(a);
        const typeOfB = TypeClassifier.INSTANCE.apply(b);
        if (typeOfA !== typeOfB) {
            return false;
        }
        switch (typeOfA) {
            case enums_1.InputType.NATIVE: return a === b;
            case enums_1.InputType.ARRAY: return this.deepEqualArray(a, b);
            case enums_1.InputType.OBJECT: return this.deepEqualObject(a, b);
            default: return false;
        }
    }
    deepEqualArray(a, b) {
        if (a.length !== b.length) {
            return false;
        }
        for (let i = 0; i < a.length; i++) {
            if (this.apply(a[i], b[i])) {
                continue;
            }
            return false;
        }
        return true;
    }
    deepEqualObject(a, b) {
        if (Object.getOwnPropertyNames(a).length !== Object.getOwnPropertyNames(b).length) {
            return false;
        }
        for (const prop in a) {
            if (prop in b && this.apply(a[prop], b[prop])) {
                continue;
            }
            return false;
        }
        return true;
    }
}
DeepEqualsFunction.INSTANCE = new DeepEqualsFunction();
////////////////////////////////////////////////////////////////////////////////
// Default values
////////////////////////////////////////////////////////////////////////////////
class IsDefaultFunction {
    constructor() {
        /* uninstantiable */
    }
    /**
     * Function to check if the input value is defined as default or not.
     * @param val value to check.
     * @param altDefRef alternative default values.
     * @returns true if and only if the input value is defined as default;
     *          otherwise returns false.
     */
    apply(val, altDefRef = null) {
        return ClassifyDefaultFunction.INSTANCE.apply(val, altDefRef) !== -1 /* NOT_A_DEFAULT_VALUE */;
    }
}
IsDefaultFunction.INSTANCE = new IsDefaultFunction();
class ClassifyDefaultFunction {
    constructor() {
        /* uninstantiable */
    }
    /**
     * Function to check if the input value is defined as default or not.
     * @param val value to check.
     * @param altDefRef alternative default values.
     * @returns true if and only if the input value is defined as default;
     *          otherwise returns false.
     */
    apply(val, altDefRef = null) {
        if (val === undefined) {
            return 0 /* UNDEFINED */;
        }
        if (val === null) {
            return 4 /* NULL */;
        }
        if (Array.isArray(val)) {
            if (altDefRef && altDefRef.length !== 0 && Array.isArray(altDefRef[0]) && DeepEqualsFunction.INSTANCE.apply(val, altDefRef[0])) {
                return -2 /* USER_DEFINED */;
            }
            return val.length === 0 ? 6 /* EMPTY_ARRAY */ : -1 /* NOT_A_DEFAULT_VALUE */;
        }
        if (!altDefRef) {
            if (typeof val === 'object') {
                if (Object.getOwnPropertyNames(val).length === 0) {
                    return 5 /* EMPTY_OBJ */;
                }
                return -1 /* NOT_A_DEFAULT_VALUE */;
            }
            if (typeof val === 'boolean') {
                return val === config_1.ConfigurationUtil.DEFAULTS.BOOLEAN ? 2 /* BOOLEAN */ : -1 /* NOT_A_DEFAULT_VALUE */;
            }
            if (typeof val === 'string') {
                return val === config_1.ConfigurationUtil.DEFAULTS.STRING ? 3 /* STRING */ : -1 /* NOT_A_DEFAULT_VALUE */;
            }
            if (typeof val === 'number') {
                return val === config_1.ConfigurationUtil.DEFAULTS.NUMBER ? 1 /* NUMBER */ : -1 /* NOT_A_DEFAULT_VALUE */;
            }
        }
        if (altDefRef) {
            for (const item of altDefRef) {
                if (DeepEqualsFunction.INSTANCE.apply(item, val)) {
                    return -2 /* USER_DEFINED */;
                }
            }
        }
        return -1 /* NOT_A_DEFAULT_VALUE */;
    }
}
ClassifyDefaultFunction.INSTANCE = new ClassifyDefaultFunction();
////////////////////////////////////////////////////////////////////////////////
/**
 * Set of functions.
 */
exports.FunctionUtil = {
    /**
     * Identity function.
     */
    IDENTITY: IdentityFunction.INSTANCE,
    /**
     * Function to compare in deep if two objects or two arrays are equals.
     */
    DEEP_EQUALS: DeepEqualsFunction.INSTANCE,
    /**
     * Function to classify the type of the value:
     *    ARRAY
     *    OBJECT
     *    NATIVE
     *    UNKNOWN
     */
    TYPE_CLASSIFIER: TypeClassifier.INSTANCE,
    /**
     * Function to check if the input value is defined as default or not.
     */
    IS_DEFAULT: IsDefaultFunction.INSTANCE,
    /**
     * Function to classify the kind of default value:
     *    USER_DEFINED
     *    NOT_A_DEFAULT_VALUE
     *    UNDEFINED
     *    ZERO
     *    FALSE
     *    EMPTY_STR
     *    NULL
     *    EMPTY_OBJ
     *    EMPTY_ARRAY
     */
    DEFAULT_VALUE_CLASSIFIER: ClassifyDefaultFunction.INSTANCE
};

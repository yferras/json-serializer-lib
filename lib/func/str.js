"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StringModifier = void 0;
class UpperCaseModifier {
    apply(arg) {
        return arg.toUpperCase();
    }
}
class LowerCaseModifier {
    apply(arg) {
        return arg.toLowerCase();
    }
}
/**
 * String modifiers.
 */
exports.StringModifier = {
    /**
     * The input string will be modified to the upper case string.
     *
     * Example:
     *    StringModifier.UPPER_CASE.apply('abcd'); // Will be modified like: 'ABCD'.
     */
    UPPER_CASE: new UpperCaseModifier(),
    /**
     * The input string will be modified to the lower case string.
     *
     * Example:
     *    StringModifier.LOWER_CASE.apply('ABCD'); // Will be modified like: 'abcd'.
     */
    LOWER_CASE: new LowerCaseModifier()
};

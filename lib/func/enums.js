"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.InputType = void 0;
var InputType;
(function (InputType) {
    /**
     * For arrays.
     * = 0
     */
    InputType[InputType["ARRAY"] = 0] = "ARRAY";
    /**
     * Only for: 'string' | 'number' | 'boolean' | 'undefined' | 'null' | 'Date'.
     * = 1
     */
    InputType[InputType["NATIVE"] = 1] = "NATIVE";
    /**
     * Only for: 'object'
     * = 2
     */
    InputType[InputType["OBJECT"] = 2] = "OBJECT";
    /**
     * Any else.
     * = 3
     */
    InputType[InputType["UNKNOWN"] = 3] = "UNKNOWN";
})(InputType = exports.InputType || (exports.InputType = {}));

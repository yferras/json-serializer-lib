"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TypeDescriptor = exports.Key = void 0;
var Key;
(function (Key) {
    Key["PROP_NAME"] = "_propName";
    Key["SCHEMA"] = "_sch";
    Key["DEFAULT_VALUE"] = "_defVal";
    Key["KEEP_DEFAULT_VALUE"] = "_keepDef";
    Key["DESERIALIZE_FUNCTION"] = "_desFunc";
    Key["SERIALIZE_FUNCTION"] = "_serFunc";
    Key["CONVERTER"] = "_conv";
    Key["DEBUGGER"] = "_debugger"; // Only for debugging.
})(Key = exports.Key || (exports.Key = {}));
var TypeDescriptor;
(function (TypeDescriptor) {
    /* eslint-disable @typescript-eslint/no-explicit-any */
    TypeDescriptor[TypeDescriptor["ARRAY"] = 't@a_'] = "ARRAY";
    TypeDescriptor[TypeDescriptor["OBJECT"] = 't@o_'] = "OBJECT";
    TypeDescriptor[TypeDescriptor["STRING"] = 't@s_'] = "STRING";
    TypeDescriptor[TypeDescriptor["NUMBER"] = 't@n_'] = "NUMBER";
    TypeDescriptor[TypeDescriptor["BOOLEAN"] = 't@b_'] = "BOOLEAN";
    TypeDescriptor[TypeDescriptor["DATE"] = 't@d_'] = "DATE";
    TypeDescriptor[TypeDescriptor["NONE"] = ''] = "NONE";
    /* eslint-disable @typescript-eslint/no-explicit-any */
})(TypeDescriptor = exports.TypeDescriptor || (exports.TypeDescriptor = {}));

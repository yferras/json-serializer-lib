"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GeneratorUtil = exports.AbstractSchemaGenerator = void 0;
const commons_1 = require("../commons");
const func_1 = require("../func");
const enums_1 = require("./enums");
const schema_1 = require("./schema");
class AbstractSchemaGenerator {
    constructor() {
        /* inherit */
    }
    /**
     * Abstraction to generate the schema.
     *
     * @param input - the input to get the schema.
     * @param modifier - to modified the schema.
     */
    apply(input, modifier = func_1.FunctionUtil.IDENTITY) {
        const skeleton = this.getSkeleton(input);
        return this.getSchema(skeleton, modifier);
    }
    /**
     * Gets the schema from the skeleton.
     *
     * @param skeleton the skeleton @see AbstractSchemaGenerator.getSkeleton
     */
    getSchema(skeleton, modifier) {
        const propNameList = [];
        const schemas = {};
        for (const property in skeleton) {
            const val = skeleton[property];
            const inputType = func_1.FunctionUtil.TYPE_CLASSIFIER.apply(val);
            switch (inputType) {
                case func_1.InputType.NATIVE:
                    propNameList.push(property);
                    break;
                case func_1.InputType.OBJECT:
                    {
                        schemas[property] = this.getSchema(val, modifier);
                        propNameList.push(property);
                    }
                    break;
                default:
                    commons_1.Logger.instance.warn(() => `${inputType} is not valid to build a schema.`);
                    break;
            }
        }
        const result = {};
        if (propNameList.length > 0) {
            const newPropNameList = this.generate(propNameList.sort(), modifier);
            for (let i = 0; i < newPropNameList.length; ++i) {
                const propName = propNameList[i];
                if (!schemas[propName]) {
                    result[propName] = newPropNameList[i];
                    continue;
                }
                const sub = {};
                sub[enums_1.Key.PROP_NAME] = newPropNameList[i];
                sub[enums_1.Key.SCHEMA] = schemas[propName];
                result[propName] = sub;
            }
        }
        return result;
    }
    /**
     * Gets a simple representation of the input.
     *
     * @param input the source from get the skeleton.
     * @param skeleton the empty skeleton.
     *
     * @returns the skeleton.
     */
    getSkeleton(input, skeleton = {}) {
        if (func_1.FunctionUtil.TYPE_CLASSIFIER.apply(input) === func_1.InputType.ARRAY) {
            const array = input;
            for (let i = 0; i < array.length; i++) {
                if (func_1.FunctionUtil.TYPE_CLASSIFIER.apply(input[i]) === func_1.InputType.NATIVE) {
                    continue;
                }
                schema_1.Schema.merge(this.getSkeleton(input[i], skeleton), skeleton);
            }
            return skeleton;
        }
        for (const property in input) {
            const val = input[property];
            const inputType = func_1.FunctionUtil.TYPE_CLASSIFIER.apply(val);
            switch (inputType) {
                case func_1.InputType.NATIVE:
                    {
                        if (val instanceof Date) {
                            skeleton[this.getPropertyName('d', property)] = '';
                        }
                        else {
                            skeleton[this.getPropertyName((typeof val)[0], property)] = '';
                        }
                    }
                    break;
                case func_1.InputType.OBJECT:
                    skeleton[this.getPropertyName('o', property)] = this.getSkeleton(val);
                    break;
                case func_1.InputType.ARRAY:
                    {
                        const aux = this.getSkeleton(val);
                        schema_1.Schema.merge(skeleton[this.getPropertyName('a', property)], aux);
                        skeleton[this.getPropertyName('a', property)] = func_1.FunctionUtil.DEEP_EQUALS.apply(aux, {}) ? '' : aux;
                    }
                    break;
                default:
                    commons_1.Logger.instance.warn(() => `${inputType} is not valid to get a skeleton.`);
                    break;
            }
        }
        return skeleton;
    }
    getPropertyName(typ, propertyName) {
        return GeneratorUtil.INCLUDE_TYPE_DESCRIPTORS ? `t@${typ}_${propertyName}` : propertyName;
    }
}
exports.AbstractSchemaGenerator = AbstractSchemaGenerator;
/**
 * Generates the names of the properties with the minimum length.
 */
class MinGenerator extends AbstractSchemaGenerator {
    constructor() {
        super();
        for (let i = 'a'.charCodeAt(0); i <= 'z'.charCodeAt(0); i++) {
            MinGenerator.SRC.push(String.fromCharCode(i));
        }
    }
    generate(propNameList, modifier) {
        const result = [];
        for (let i = 0; i < propNameList.length; i++) {
            let newName = this.encode(i);
            let count = 1;
            while (result.indexOf(newName) !== -1) {
                newName = this.encode(i + count++);
            }
            result.push(modifier.apply(this.encode(i)));
        }
        return result;
    }
    encode(num) {
        let result = '';
        let c = false;
        while (num >= MinGenerator.SRC.length) {
            result += MinGenerator.SRC[num % MinGenerator.SRC.length] + ',';
            num = Math.floor(num / MinGenerator.SRC.length);
            c = true;
        }
        const r = num % MinGenerator.SRC.length;
        result += MinGenerator.SRC[c ? r - 1 : r];
        return result.split(',').reverse().join('');
    }
}
MinGenerator.SRC = [];
/**
 * Using regular expressions.
 */
class FromRegexGenerator extends AbstractSchemaGenerator {
    constructor(regexp) {
        super();
        this.regexp = regexp;
        this.regexp = regexp;
    }
    generate(propNameList, modifier) {
        const result = [];
        let count = 1;
        for (const item of propNameList) {
            let str = item.replace(/^t@\w_/, '');
            str = str.replace(this.regexp, ',')
                .split(',')
                .map(x => x.replace(/\W|_/g, ''))
                .map(x => x.charAt(0))
                .join('');
            let newName = modifier.apply(str);
            while (result.indexOf(newName) !== -1) {
                newName += count++;
            }
            result.push(newName);
        }
        return result;
    }
}
/**
 * Utility class to join all implemented schema generators.
 */
class GeneratorUtil {
}
exports.GeneratorUtil = GeneratorUtil;
GeneratorUtil.INCLUDE_TYPE_DESCRIPTORS = true;
/**
 * Generates the names of the properties with the minimum length.
 */
// eslint-disable-next-line
GeneratorUtil.MIN_LENGTH = new MinGenerator();
/**
 * Transforms the original property name into a shorter version.
 */
// eslint-disable-next-line
GeneratorUtil.DEFAULT = new FromRegexGenerator(/(?<=[a-zA-Z0-9])(?=[A-Z_]|\W)/g);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Logger = exports.LogLevel = exports.DeserializedOutput = exports.SerializedOutput = void 0;
/**
 * Abstraction of output.
 * @param <T> any type.
 */
class AbstractOutput {
    constructor(result, stats) {
        this._result = result;
        this._stats = stats;
    }
    get stats() {
        return this._stats;
    }
    get result() {
        return this._result;
    }
}
/**
 * Representation of a collapsed/serialized output.
 * @param <T> any type.
 */
class SerializedOutput extends AbstractOutput {
    constructor(result, stats) {
        super(result, stats);
    }
    get serializedJSON() {
        return this.result;
    }
}
exports.SerializedOutput = SerializedOutput;
/**
 * Representation of an expanded/deserialized output.
 * @param <T> any type.
 */
class DeserializedOutput extends AbstractOutput {
    constructor(result, stats) {
        super(result, stats);
    }
    get deserializedJSON() {
        return this.result;
    }
}
exports.DeserializedOutput = DeserializedOutput;
////////////////////////////////////////////////////////////////////////////////
// Print Logs
////////////////////////////////////////////////////////////////////////////////
var LogLevel;
(function (LogLevel) {
    LogLevel[LogLevel["NONE"] = 0] = "NONE";
    LogLevel[LogLevel["TRACE"] = 1] = "TRACE";
    LogLevel[LogLevel["DEBUG"] = 2] = "DEBUG";
    LogLevel[LogLevel["INFO"] = 4] = "INFO";
    LogLevel[LogLevel["WARN"] = 8] = "WARN";
    LogLevel[LogLevel["ERROR"] = 16] = "ERROR";
    LogLevel[LogLevel["ALL"] = 32] = "ALL";
})(LogLevel = exports.LogLevel || (exports.LogLevel = {}));
const LogLevelFunction = {
    [LogLevel.TRACE]: console.trace,
    [LogLevel.DEBUG]: console.debug,
    [LogLevel.INFO]: console.info,
    [LogLevel.WARN]: console.warn,
    [LogLevel.ERROR]: console.error,
};
class Logger {
    constructor(logLevel = LogLevel.WARN) {
        this.map = {};
        switch (logLevel) {
            case LogLevel.DEBUG:
            case LogLevel.TRACE:
            case LogLevel.INFO:
            case LogLevel.WARN:
            case LogLevel.ERROR:
                this.map[logLevel] = LogLevelFunction[logLevel];
                break;
            case LogLevel.ALL:
                {
                    this.map[LogLevel.TRACE] = LogLevelFunction[LogLevel.TRACE];
                    this.map[LogLevel.DEBUG] = LogLevelFunction[LogLevel.DEBUG];
                    this.map[LogLevel.INFO] = LogLevelFunction[LogLevel.INFO];
                    this.map[LogLevel.WARN] = LogLevelFunction[LogLevel.WARN];
                    this.map[LogLevel.ERROR] = LogLevelFunction[LogLevel.ERROR];
                }
                break;
            case LogLevel.NONE:
                // NOTHING TO DO
                break;
            default:
                {
                    const array = [1];
                    for (let i = 2; i <= logLevel; i = i << 1) {
                        i in LogLevelFunction && array.unshift(i);
                    }
                    let diff = logLevel;
                    for (let i = 0; i < array.length; i++) {
                        if (diff >= array[i]) {
                            diff -= array[i];
                            this.map[array[i]] = LogLevelFunction[array[i]];
                        }
                    }
                    if (diff === 1) {
                        this.map[LogLevel.TRACE] = LogLevelFunction[LogLevel.TRACE];
                        break;
                    }
                }
                break;
        }
    }
    static setLevel(level) {
        Logger.INSTANCE = new Logger(level);
    }
    static get instance() {
        return Logger.INSTANCE;
    }
    trace(message) {
        this.log(LogLevel.TRACE, message);
    }
    debug(message) {
        this.log(LogLevel.DEBUG, message);
    }
    info(message) {
        this.log(LogLevel.INFO, message);
    }
    warn(message) {
        this.log(LogLevel.WARN, message);
    }
    error(message) {
        this.log(LogLevel.ERROR, message);
    }
    log(level, message) {
        if (level in this.map) {
            this.map[level](message());
        }
    }
}
exports.Logger = Logger;
Logger.INSTANCE = new Logger();

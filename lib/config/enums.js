"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StrategyForUnknownProperty = void 0;
/**
 * Enum to define how to process the unknown properties.
 */
var StrategyForUnknownProperty;
(function (StrategyForUnknownProperty) {
    /**
     * If any unknown property appears, then throw an error;
     */
    StrategyForUnknownProperty[StrategyForUnknownProperty["THROW_ERROR"] = 0] = "THROW_ERROR";
    /**
     * Do not put the unknown property in the final result.
     */
    StrategyForUnknownProperty[StrategyForUnknownProperty["IGNORE"] = 1] = "IGNORE";
    /**
     * Put the unknown property in the final result.
     */
    StrategyForUnknownProperty[StrategyForUnknownProperty["KEEP"] = 2] = "KEEP";
})(StrategyForUnknownProperty = exports.StrategyForUnknownProperty || (exports.StrategyForUnknownProperty = {}));

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeserializationConfig = exports.SerializationConfig = exports.AbstractConfig = void 0;
const func_1 = require("../func");
class AbstractConfig {
    constructor() {
        this._includeStats = null;
        this._includeDefaultValues = null;
        this._defaultValues = [];
        this._includeEmptyArrays = null;
        this._includeEmptyObjects = null;
        this._includeNullValues = null;
        this._includeUndefinedValues = null;
        this._includeDefaultBoolean = null;
        this._includeDefaultNumber = null;
        this._includeDefaultString = null;
        this._includeUserDefinedDefaultValues = null;
        this._strategyForUnknownProperty = null;
        this._converterMap = {
            't@b_': func_1.FunctionUtil.IDENTITY,
            't@d_': func_1.FunctionUtil.IDENTITY
        };
    }
    get includeDefaultValues() {
        return this._includeDefaultValues;
    }
    set includeDefaultValues(val) {
        this._includeDefaultValues = val;
    }
    get includeStats() {
        return this._includeStats;
    }
    set includeStats(val) {
        this._includeStats = val;
    }
    get defaultValues() {
        return this._defaultValues;
    }
    get includeEmptyArrays() {
        return this._includeEmptyArrays;
    }
    set includeEmptyArrays(val) {
        this._includeEmptyArrays = val;
    }
    get includeEmptyObjects() {
        return this._includeEmptyObjects;
    }
    set includeEmptyObjects(val) {
        this._includeEmptyObjects = val;
    }
    get includeNullValues() {
        return this._includeNullValues;
    }
    set includeNullValues(val) {
        this._includeNullValues = val;
    }
    get includeUndefinedValues() {
        return this._includeUndefinedValues;
    }
    set includeUndefinedValues(val) {
        this._includeUndefinedValues = val;
    }
    get includeDefaultBoolean() {
        return this._includeDefaultBoolean;
    }
    set includeDefaultBoolean(val) {
        this._includeDefaultBoolean = val;
    }
    get includeDefaultNumber() {
        return this._includeDefaultNumber;
    }
    set includeDefaultNumber(val) {
        this._includeDefaultNumber = val;
    }
    get includeDefaultString() {
        return this._includeDefaultString;
    }
    set includeDefaultString(val) {
        this._includeDefaultString = val;
    }
    get includeUserDefinedDefaultValues() {
        return this._includeUserDefinedDefaultValues;
    }
    set includeUserDefinedDefaultValues(val) {
        this._includeUserDefinedDefaultValues = val;
    }
    get strategyForUnknownProperty() {
        return this._strategyForUnknownProperty;
    }
    set strategyForUnknownProperty(val) {
        this._strategyForUnknownProperty = val;
    }
    get converterMap() {
        return this._converterMap;
    }
}
exports.AbstractConfig = AbstractConfig;
class SerializationConfig extends AbstractConfig {
    constructor() {
        super(...arguments);
        this._booleanConverter = func_1.FunctionUtil.IDENTITY;
        this._dateConverter = func_1.FunctionUtil.IDENTITY;
    }
    get fromBooleanTo() {
        return this._booleanConverter;
    }
    set fromBooleanTo(val) {
        this._booleanConverter = val;
        this._converterMap['t@b_'] = val;
    }
    get fromDateTo() {
        return this._dateConverter;
    }
    set fromDateTo(val) {
        this._dateConverter = val;
        this._converterMap['t@d_'] = val;
    }
}
exports.SerializationConfig = SerializationConfig;
class DeserializationConfig extends AbstractConfig {
    constructor() {
        super(...arguments);
        this._booleanConverter = func_1.FunctionUtil.IDENTITY;
        this._dateConverter = func_1.FunctionUtil.IDENTITY;
        this._useNullInsteadOfDefaulValues = false;
    }
    get toBooleanFrom() {
        return this._booleanConverter;
    }
    set toBooleanFrom(val) {
        this._booleanConverter = val;
        this._converterMap['t@b_'] = val;
    }
    get toDateFrom() {
        return this._dateConverter;
    }
    set toDateFrom(val) {
        this._dateConverter = val;
        this._converterMap['t@d_'] = val;
    }
    get useNullInsteadOfDefaulValues() {
        return this._useNullInsteadOfDefaulValues;
    }
    set useNullInsteadOfDefaulValues(v) {
        this._useNullInsteadOfDefaulValues = v;
    }
}
exports.DeserializationConfig = DeserializationConfig;
